﻿delete from ar_inv_det;
alter SEQUENCE ar_inv_det_det_id_seq  restart with 1;
delete from ar_inv ;
alter SEQUENCE ar_inv_id_seq  restart with 1;

delete from ap_inv_det;
alter SEQUENCE ap_inv_det_det_id_seq  restart with 1;
delete from ap_inv  ;
alter SEQUENCE ap_inv_id_seq  restart with 1;

delete from opname_detail    ;
alter SEQUENCE opname_detail_seq_seq restart with 1; 
delete from opname;
alter SEQUENCE opname_id_seq  restart with 1;

delete from item_history;
alter SEQUENCE item_history_itemhistid_seq  restart with 1;

update m_item  set stok =0, hpp=0;
 