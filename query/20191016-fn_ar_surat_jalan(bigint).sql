﻿CREATE OR REPLACE FUNCTION public.fn_ar_surat_jalan(bigint)
  RETURNS SETOF record AS
$BODY$
declare
	v_id	alias for $1;
	rcd	record;
	v_jml_item int;
	v_empty_row	int;
	v_baris_per_page int;
begin
v_baris_per_page=12;

select into v_jml_item count(*) from ar_inv_det where ar_id=v_id;
v_empty_row = case when v_jml_item < v_baris_per_page then 6-v_jml_item else 6 - mod(v_jml_item, v_baris_per_page) end;

for rcd in
	select h.id_customer, coalesce(h.invoice_no,'') as invoice_no, 
	coalesce(sj.kirim_ke, coalesce(c.nama_relasi,'')) as nama_customer, 
	coalesce(sj.alamat, coalesce(c.alamat,'')) as alamat, coalesce(sj.telepon,'') as telepon,
	--coalesce(sj.id_kota, c.id_kota::varchar, '')) as id_kota, 
	coalesce(k.nama_kota,'') as nama_kota, 
	coalesce(h.id_expedisi::varchar,'') as id_Expedisi, coalesce(e.nama_expedisi,'') as nama_expedisi, 
	coalesce(sj.no_sj,'') as no_sj, sj.tgl_sj, coalesce(sj.hormat_kami,'') as hormat_kami, 
	d.id_barang, coalesce(i.plu,'') as plu, coalesce(i.nama_barang,'') as nama_barang, coalesce(d.qty,0) as qty, coalesce(i.satuan,'') as satuan
	from ar_inv h  
	inner join m_relasi c on c.id_relasi=h.id_customer 
	inner join ar_inv_Det d on d.ar_id=h.id
	inner join m_item i on i.id=d.id_barang
	left join m_expedisi e on e.id=h.id_expedisi 
	left join ar_sj sj on sj.ar_id=h.id 
	left join m_kota k on k.id=case when sj.no_sj is not null then sj.id_kota else c.id_kota end 
	where sj.ar_id=v_id
	order by d.seq
LOOP
	return next rcd;
END LOOP;

FOR i IN 1..v_empty_row LOOP
    return QUERY select h.id_customer, coalesce(h.invoice_no,'') as invoice_no, 
	coalesce(sj.kirim_ke, coalesce(c.nama_relasi,'')) as nama_customer, 
	coalesce(sj.alamat, coalesce(c.alamat,'')) as alamat, coalesce(sj.telepon,'') as telepon,
	coalesce(k.nama_kota,'') as nama_kota, 
	coalesce(h.id_expedisi::varchar,'') as id_Expedisi, coalesce(e.nama_expedisi,'') as nama_expedisi, 
	coalesce(sj.no_sj,'') as no_sj, sj.tgl_sj, coalesce(sj.hormat_kami,'') as hormat_kami, 
	0::int id_barang, ''::varchar plu, ''::varchar as nama_barang, 0::double precision as qty, ''::varchar as satuan
	from ar_inv h  
	inner join m_relasi c on c.id_relasi=h.id_customer 
	left join m_expedisi e on e.id=h.id_expedisi 
	left join ar_sj sj on sj.ar_id=h.id 
	left join m_kota k on k.id=case when sj.no_sj is not null then sj.id_kota else c.id_kota end 
	where sj.ar_id=v_id;
END LOOP;

/*
select * from fn_ar_surat_jalan(2) as (id_customer bigint, invoice_no varchar, kirim_ke_nama varchar, kirim_ke_alamat text, telepon varchar, kirim_ke_kota varchar, 
id_expedisi varchar, nama_expedisi varchar, no_sj varchar, tgl_sj date, hormat_kami varchar, i_barang integer, plu varchar, nama_barang varchar, qty double precision, 
satuan varchar)
*/
end
$BODY$
  LANGUAGE plpgsql VOLATILE;
  