﻿select i.id, i.invoice_no, sum(d.qty*d.unit_price) as total, 
r.retur, r.invoice_no
from ar_inv  i 
inner join ar_inv_det d on d.ar_id=i.id
inner join 
(select h.invoice_no, h.id, sum(d.qty*d.unit_price) retur
	from ar_inv  h 
	inner join ar_inv_det d on d.ar_id=h.id
	group by h.invoice_no, h.id
) r on r.id=i.retur_dari
where trx_type='PJL'
group by i.id, i.invoice_no, r.retur, r.invoice_no
order by i.id desc , i.invoice_no, r.retur, r.retur, r.invoice_no
--select * from ar_inv

