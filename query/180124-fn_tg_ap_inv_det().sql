﻿-- Function: public.fn_tg_ap_inv_det()

-- DROP FUNCTION public.fn_tg_ap_inv_det();

CREATE OR REPLACE FUNCTION public.fn_tg_ap_inv_det()
  RETURNS trigger AS
$BODY$
declare
	r_apinv	record;
	r_tipe_harga_jual record;
	v_priority int;
	v_tot_qty_po	double precision;
	v_tot_qty_gr	double precision;
	v_hpp		double precision;
	v_qty_stok_ak	double precision;
	v_tot_stok_ak	double precision;
	v_round_up	double precision;
	v_harga_terakhir double precision;
	v_harga_trx	double precision;
	 
begin
if(TG_OP='INSERT' or TG_OP='UPDATE') then
	v_harga_trx = coalesce(fn_get_Disc_Bertingkat(coalesce(NEw.unit_price,0), coalesce(NEw.disc,''))*(1+coalesce(New.ppn,0)/100));
	
	select into v_harga_terakhir coalesce(fn_get_Disc_Bertingkat(coalesce(unit_price,0), coalesce(disc,''))*(1+coalesce(ppn,0)/100))
	from ap_inv_det where id_barang=new.id_barang order by det_id desc limit 1;
	v_harga_terakhir=coalesce(v_harga_terakhir,0);
	
	select into r_apinv i.*, t.keterangan, s.nama_relasi as nama_supplier from ap_inv i
	left join m_trx_type t on t.kode=i.trx_type 
	left join m_relasi s on s.id_relasi=i.supplier_id where id=NEW.ap_id;

	select into v_qty_stok_ak, v_tot_stok_ak 
	sum(coalesce(quantity,0)), sum(coalesce(quantity,0)) * coalesce(i.hpp,0) 
	from item_history ih inner join m_item i on i.id=ih.id_barang
	where i.id=NEW.id_barang group by coalesce(i.hpp,0) ;

	update m_item set hpp=
	case when (coalesce(v_qty_stok_ak,0)+NEW.qty)=0 then coalesce(NEW.qty*fn_get_Disc_Bertingkat(coalesce(NEw.unit_price,0), coalesce(NEw.disc,''))*(1+coalesce(New.ppn,0)/100), 0)
	else (coalesce(v_tot_stok_ak,0)+coalesce(NEW.qty*fn_get_Disc_Bertingkat(coalesce(NEw.unit_price,0), coalesce(NEw.disc,''))*(1+coalesce(New.ppn,0)/100), 0)) / (coalesce(v_qty_stok_ak,0)+NEW.qty)
	end
	where id=NEW.id_barang;
	raise notice 'v_harga_trx %', v_harga_trx;
	raise notice 'v_harga_terakhir %', v_harga_terakhir;
	
	--if(v_harga_terakhir::numeric<>v_harga_trx) then
		select into v_round_up round_up from m_setting;
		v_round_up=coalesce(v_round_up, 500);

		for r_tipe_harga_jual in
			select t.id, coalesce(h.margin, t.margin_default) margin_default 
			from m_tipe_harga_jual t 
			left join m_item_harga_jual h on t.id=h.id_tipe_harga_jual and h.id_item=new.id_barang
		LOOP
			if exists(select * from m_item_harga_jual where id_item =new.id_barang and id_tipe_harga_jual=r_tipe_harga_jual.id) then
				update m_item_harga_jual set harga_jual=ceil(v_harga_trx*(1+coalesce(r_tipe_harga_jual.margin_default,0)/100)::numeric / v_round_up)*v_round_up
				where id_item=new.id_barang and id_tipe_harga_jual=r_tipe_harga_jual.id;
			else	
				insert into m_item_harga_jual(id_item, id_tipe_harga_jual, margin, harga_jual) VALUES
				(new.id_barang, r_tipe_harga_jual.id, r_tipe_harga_jual.margin_default, 
				ceil(v_harga_trx*(1+coalesce(r_tipe_harga_jual.margin_default,0)/100)::numeric / v_round_up)*v_round_up);
			end if;
			
		END LOOP;	
	--end if;

	select into v_tot_qty_gr sum(qty) from ap_inv_det d
	inner join ap_inv h on h.id=ap_id and h.no_po=r_apinv.no_po;

	select into v_tot_qty_po sum(qty) from po_Det where no_po=r_apinv.no_po;

	if(v_tot_qty_gr>=v_tot_qty_po) then
		update po set closed=true where no_po=r_apinv.no_po;
	end if;	
	if(exists(select * from m_item_supplier where id_barang = NEW.id_barang and  id_supplier=r_apinv.supplier_id)) then
		UPDATE m_item_supplier
		   SET harga=NEW.unit_price, disc=NEW.disc, ppn=NEw.ppn
		WHERE id_barang = NEW.id_barang and  id_supplier=r_apinv.supplier_id;
	else
		select into v_priority max(prioritas) from m_item_supplier where id_barang = NEW.id_barang and  id_supplier=r_apinv.supplier_id;
		INSERT INTO m_item_supplier(
		id_barang, id_supplier, harga, disc, ppn, prioritas)
		VALUES (NEW.id_barang, r_apinv.supplier_id, NEw.unit_price, NEW.disc, NEW.ppn, v_priority);
	end if;

	if(TG_OP='INSERT') then
		
		
		INSERT INTO item_history(
			    id_barang, txdate, txtype, invoiceid, description, 
			    quantity, sellingprice, item_cost, costavr, prevcost, prevqty, 
			    glperiod, glyear, warehouseid, fifohistid, fifostart, qtycontrol, no_bukti, det_id)
		select new.id_barang, r_apinv.invoice_date, r_apinv.trx_type, r_apinv.id, r_apinv.keterangan||' - '||r_apinv.nama_supplier , 
			    NEW.qty, 0, fn_get_Disc_Bertingkat(coalesce(NEw.unit_price,0), coalesce(NEw.disc,''))*(1+coalesce(New.ppn,0)/100), 0, 0, 0, 
			    to_char(r_apinv.invoice_date, 'MM')::smallint, to_char(r_apinv.invoice_date, 'YYYY')::int, r_apinv.id_gudang, null, 
			    New.qty, New.qty, r_apinv.invoice_no, NEW.det_id
		from m_Item i where i.id=NeW.id_barang;
		return NEW;
	end if;		
	if(TG_OP='UPDATE') then
		UPDATE item_history 
			set 	id_barang=new.id_barang, 
				txdate=r_apinv.invoice_date, 
				txtype=r_apinv.trx_type, 
				invoiceid=r_apinv.id, 
				description=r_apinv.keterangan||' '||i.nama_barang, 
				quantity=NEW.qty, 
				sellingprice=0, 
				item_cost=fn_get_Disc_Bertingkat(coalesce(NEw.unit_price,0), coalesce(NEw.disc,''))*(1+coalesce(New.ppn,0)/100), 
				costavr=0, 
				prevcost=0, 
				prevqty=0, 
				glperiod=to_char(r_apinv.invoice_date, 'MM')::smallint, 
				glyear=to_char(r_apinv.invoice_date, 'YYYY')::int, 
				warehouseid=r_apinv.id_gudang, 
				fifohistid=null, 
				fifostart=New.qty, 
				qtycontrol=New.qty, 
				no_bukti=r_apinv.invoice_no,
				det_id=NEW.det_id
		from m_Item i where i.id=NeW.id_barang 
		and invoiceid=r_apinv.id and id_barang=new.id_barang and txtype=r_apinv.trx_type;

		return NEW;
	end if;

	RETURN NULL;
end if;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.fn_tg_ap_inv_det()
  OWNER TO postgres;
