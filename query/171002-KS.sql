﻿create or replace function fn_rpt_kartu_stok_formula(varchar, varchar, varchar, varchar)
returns setof record
as
$$
declare
	v_item 	alias for $1;
	v_gudang	alias for $2;
	v_tanggal1	alias for $3;
	v_tanggal2	alias for $4;
	rcd	record;
	v_saldo_total	double precision;
	v_saldo_gudang	double precision;
	v_litem	int;
	v_lgudang int;
	
begin
for rcd in
	select h.id_barang, coalesce(i.plu,'') plu, coalesce(i.nama_barang, '') nama_barang, h.warehouseid id_gudang, 
	coalesce(g.nama_gudang,'') nama_gudang, h.txdate tanggal, h.txtype, coalesce(h.no_bukti,'') no_bukti, 
	h.description keterangan, coalesce(h.sellingprice,0) sellingprice, coalesce(h.item_cost,0) item_cost, 
	coalesce(h.quantity,0) quantity, h.itemhistid
	from item_history h
	inner join m_item i on i.id=h.id_barang
	left join m_gudang g on g.id=h.warehouseid
	where case when v_item='' then true else id_barang=v_item::int  end
	AND case when v_gudang='' then true else h.warehouseid=v_gudang::int end
	and h.txdate>=v_tanggal1::date
	and h.txdate<=v_tanggal2::date
	order by h.warehouseid, h.txdate, h.itemhistid
	
LOOP
	raise notice 'v_litem: %, itemskg: %, lgudang: %, gdgskg: %', v_litem, rcd.id_barang, v_lgudang, rcd.id_gudang;
	
	if(coalesce(v_litem,0)<>rcd.id_barang) then
		v_saldo_total=0;
		v_litem = rcd.id_barang;
	end if;
	if(coalesce(v_lgudang,0)<>rcd.id_gudang) then
		v_saldo_gudang=0;
		v_lgudang=rcd.id_gudang;
	end if;
	v_saldo_total=v_saldo_total+coalesce(rcd.quantity,0);
	v_saldo_gudang=v_saldo_gudang+coalesce(rcd.quantity,0);
	
	return QUERY select rcd.id_barang, rcd.plu, rcd.nama_barang, rcd.id_gudang, rcd.nama_gudang, 
	rcd.tanggal, rcd.txtype, rcd.no_bukti, rcd.keterangan, rcd.sellingprice, rcd.item_cost, rcd.quantity, 
	rcd.itemhistid, 
	case when rcd.quantity>0 then rcd.quantity else 0 end masuk, 
	case when rcd.quantity<0 then abs(rcd.quantity) else 0 end keluar, 
	v_saldo_gudang, v_saldo_total;
	
END LOOP;		
/*
select * from fn_rpt_kartu_stok_formula(1026, 0, '2017-01-01', '2017-10-01') as (id_barang integer, plu varchar, nama_barang varchar, id_gudang integer,
nama_gudang varchar, tanggal date, trx_type varchar, no_bukti varchar, keterangan varchar, selling_price double precision, item_cost double precision,
quantity double precision, id bigint, masuk double precision, keluar double precision, saldo_gudang double precision, 
saldo_total double precision)

select id_barang, count(distiNct warehouseid), COUNT(*) from item_history
group by id_barang
having count(distiNct warehouseid) > 1
*/
end
$$
language 'plpgsql';
