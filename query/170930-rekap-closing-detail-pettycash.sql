﻿-- Function: public.fn_rpt_closing_shift(character varying, character varying)

-- DROP FUNCTION public.fn_rpt_closing_shift(character varying, character varying);

CREATE OR REPLACE FUNCTION public.fn_rpt_closing_shift(
    v_tanggal character varying,
    v_shift character varying)
  RETURNS SETOF record AS
$BODY$
declare
	rcd record;
begin
for rcd in	
	select urut, tipe, sum(total)
	from(
		select 1 urut, 'PENJUALAN'tipe, 
		coalesce(sum(d.qty*d.unit_price),0) total
		from ar_inv h 
		inner join ar_inv_det d on d.ar_id=h.id
		where h.invoice_date=v_tanggal::date
		and case when v_shift='' then true else h.shift=v_shift end
		and trx_type='PJL'
		
		union all
		
		select 2 urut, 'POTONGAN PENJUALAN' tipe, 
		coalesce(-sum(case when isnumeric(d.disc) then d.qty*d.disc::double precision else 0 end),0) total
		from ar_inv h 
		inner join ar_inv_det d on d.ar_id=h.id
		where h.invoice_date=v_tanggal::date
		and case when v_shift='' then true else h.shift=v_shift end and coalesce(d.disc,'')<>''
		and trx_type='PJL'
		and isnumeric(d.disc) and d.disc::numeric > 0
		
		union all

		select 1 urut, 'RETUR PENJUALAN'tipe, 
		sum(d.qty*d.unit_price-case when isnumeric(d.disc) then d.qty*d.disc::double precision else 0 end) total
		from ar_inv h 
		inner join ar_inv_det d on d.ar_id=h.id
		where h.invoice_date=v_tanggal::date
		and h.trx_type='RJL'
		and case when v_shift='' then true else h.shift=v_shift end
		group by case when trx_type='PJL' then 1 else 3 end, case when trx_type='PJL' then 'PENJUALAN' when trx_type='RJL' then 'RETUR PENJUALAN' else '' end

		union all

		select  case when h.masuk>0 then 9 else 10 end, 'PETTY CASH'||case when h.masuk>0 then ' MASUK' else ' KELUAR' end ||' ('||h.keterangan||')' tipe, 
		sum(coalesce(h.masuk,0)-coalesce(h.keluar,0)) total
		from petty_cash h 
		where h.tanggal=v_tanggal::date
		and case when v_shift='' then true else h.shift=v_shift end
		group by case when h.masuk>0 then 9 else 10 end, 'PETTY CASH'||case when h.masuk>0 then ' MASUK' else ' KELUAR' end
		
		union all

		select 3 urut, 'PENERIMAAN PIUTANG PELANGGAN', coalesce(sum(coalesce(d.bayar,0)-coalesce(d.discount,0)),0) 
		from ar_receipt h
		inner join ar_receipt_detail d on h.ar_no=d.ar_no  
		where h.tanggal=v_tanggal::date
		and case when v_shift='' then true else h.shift=v_shift end
				
	)x
	group by urut, tipe
	order by urut 
LOOP
	return next rcd;
END LOOP;
/*
select * from fn_rpt_closing_shift('2017-09-19', '') as (urut integer, tipe text, total double precision)
select * from ar_inv  order by invoice_date   desc
*/	
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public.fn_rpt_closing_shift(character varying, character varying)
  OWNER TO postgres;
