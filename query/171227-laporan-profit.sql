﻿-- Function: public.fn_rpt_penjualan_detail_item_cost(character varying, character varying, bigint, bigint, bigint, bigint)

-- DROP FUNCTION public.fn_rpt_penjualan_detail_item_cost(character varying, character varying, bigint, bigint, bigint, bigint);

CREATE OR REPLACE FUNCTION public.fn_rpt_penjualan_detail_item_cost(
    character varying,
    character varying,
    bigint,
    bigint,
    bigint,
    bigint)
  RETURNS SETOF record AS
$BODY$
declare
	v_tanggal1	alias for $1;
	v_Tanggal2	alias for $2;
	v_cust		alias for $3;
	v_sales		alias for $4;
	v_expedisi	alias for $5;
	v_gudang	alias for $6;
	rcd 		record;
begin
for rcd in
	select i.id, i.invoice_no as no_faktur, (i.invoice_date::date) as tgl_faktur, coalesce(i.no_so, '') as no_so, coalesce(g.nama_gudang,'') as nama_gudang, 
	i.id_customer, coalesce(c.nama_relasi,'') as nama_customer, coalesce(c.alamat,'') as alamat_cust, coalesce(k.nama_kota,'') as kota_customer, 
	case when coalesce(c.telp,'')<>'' then c.telp else coalesce(c.hp,'') end as telp_hp, i.id_expedisi, coalesce(ex.nama_expedisi,'') as nama_expedisi,
	(i.invoice_date+coalesce(i.top,0)) as jt_tempo, coalesce(i.description,'') as ket_faktur, coalesce(t.keterangan,'') as trx_type, coalesce(i.ket_pembayaran,'') as ket_pembayaran,
	case when i.jenis_bayar='K' then 'Kredit' else 'Tunai' end as status_bayar, i.id_sales, coalesce(s.nama_sales,'') as nama_sales, 
	coalesce(i.ar_disc,'') as ar_disc, 
	--coalesce(x.disc_rp, 0) as ar_disc_rp, coalesce(x.nett,0) as nett, 
	0::double precision ar_disc_rp, 0::double precision nett,
	coalesce(m_item.plu,'') as plu, coalesce(m_item.nama_barang,'') as nama_barang, 
	-1*ks.quantity, coalesce(m_item.satuan,'') as satuan, d.unit_price, case when isnumeric(d.disc) then d.disc else '0' end disc, d.ppn, coalesce(d.biaya,0) as biaya,
	--fn_get_Disc_Bertingkat(coalesce(d.qty,0)*coalesce(d.unit_price,0), coalesce(d.disc,''))*(1+coalesce(d.ppn,0)/100)+coalesce(d.biaya,0) as sub_total, 
	(d.qty*(coalesce(d.unit_price,0)-case when isnumeric(d.disc) then d.disc::numeric else 0 end))*(1+coalesce(d.ppn,0)/100)+coalesce(d.biaya,0) as sub_total,
	coalesce(d.keterangan,'') as keterangan, 
	ks.item_cost
	from ar_inv  i
	inner join ar_inv_det d on d.ar_id=i.id
	inner join m_item on m_item.id=d.id_barang
	inner join item_history ks on ks.invoiceid=d.ar_id and ks.txtype=i.trx_type and ks.id_barang=d.id_barang --ks.det_id=d.det_id 
	left join m_relasi c on c.id_relasi=i.id_customer
	left join m_gudang g on g.id=i.id_gudang
	left join m_salesman s on s.id_sales=i.id_sales
	left join m_expedisi ex on ex.id=i.id_expedisi
	left join m_user u on u.user_name=i.user_ins
	left join m_kota k on k.id=c.id_kota
	left join m_trx_type t on t.kode=i.trx_type
	/*left join(
		select h.id, 
		--fn_get_Disc_Bertingkat(sum(fn_get_Disc_Bertingkat(coalesce(d.qty,0)* coalesce(d.unit_price,0), coalesce(d.disc,''))*(1+coalesce(d.ppn,0)/100)), coalesce(h.ar_disc,''))+coalesce(h.biaya_lain,0) as nett, 
		--sum(fn_get_Disc_Bertingkat(coalesce(d.qty,0)*coalesce(d.unit_price,0), coalesce(d.disc,''))*(1+coalesce(d.ppn,0)/100))-
		--fn_get_Disc_Bertingkat(sum(fn_get_Disc_Bertingkat(coalesce(d.qty,0)*coalesce(d.unit_price,0), coalesce(d.disc,''))*(1+coalesce(d.ppn,0)/100)), coalesce(h.ar_disc,'')) as disc_rp
		sum((d.qty*(coalesce(d.unit_price,0)-case when isnumeric(d.disc) then d.disc::numeric else 0 end))*(1+coalesce(d.ppn,0)/100)+coalesce(d.biaya,0)) as nett,
		case when isnumeric(d.disc) then d.disc::numeric else 0 end disc_rp
		from ar_inv h
		inner join ar_inv_det d on d.ar_id=h.id 
		where to_Char(h.invoice_date, 'yyyy-MM-dd')>=v_tanggal1
		and to_Char(h.invoice_date, 'yyyy-MM-dd')<=v_tanggal2
		and case when v_cust is not null or v_cust>0 then h.id_customer=v_cust else 1=1 end
		and case when v_sales is not null or v_sales>0 then h.id_sales=v_sales else 1=1 end
		and case when v_expedisi is not null or v_expedisi>0 then h.id_expedisi=v_expedisi else 1=1 end
		and case when v_gudang is not null or v_gudang>0 then h.id_gudang=v_gudang else 1=1 end
		group by h.id, coalesce(h.ar_disc,'')

		
	)x on x.id=i.id
	*/
	where i.invoice_date::date>=v_tanggal1::date
	and i.invoice_date::date<=v_tanggal2::date
	and case when v_cust is not null or v_cust>0 then i.id_customer=v_cust else 1=1 end
	and case when v_sales is not null or v_sales>0 then i.id_sales=v_sales else 1=1 end
	and case when v_expedisi is not null or v_expedisi>0 then i.id_expedisi=v_expedisi else 1=1 end
	and case when v_gudang is not null or v_gudang>0 then i.id_gudang=v_gudang else 1=1 end
	order by i.id, d.det_id
LOOP
	return next rcd;
END LOOP;	
/*
select * from fn_rpt_penjualan_detail_item_cost('2017-12-20', '2017-12-31', null, null, null, null) as (id_faktur bigint, no_faktur varchar, tgl_faktur date, no_so varchar, 
nama_gudang varchar, id_customer bigint, nama_custoer varchar, alamat_customer varchar, kota_customer varchar, telp_hp varchar, id_expedisi int, 
nama_expedisi varchar, jt_tempo date, ket_faktur text, trx_type varchar, ket_pembayaran text, status_bayar text, id_sales bigint, nama_sales varchar, ar_disc varchar, 
ar_disc_rp double precision, nett double precision, plu varchar, nama_barang varchar, qty double precision, satuan varchar, unit_price double precision, 
disc varchar, ppn double precision, biaya double precision, sub_total double precision, keterangan varchar, item_cost double precision)

*/
return;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public.fn_rpt_penjualan_detail_item_cost(character varying, character varying, bigint, bigint, bigint, bigint)
  OWNER TO postgres;
