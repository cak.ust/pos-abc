﻿--select * into item_history_tmp from item_history  --15885
--select count(*) from item_history
--select count(*) from ar_inv_det  -13903

CREATE OR REPLACE FUNCTION public.fn_rpt_kartu_stok_formula(
    character varying,
    character varying,
    character varying,
    character varying)
  RETURNS SETOF record AS
$BODY$
declare
	v_item 	alias for $1;
	v_gudang	alias for $2;
	v_tanggal1	alias for $3;
	v_tanggal2	alias for $4;
	rcd	record;
	v_saldo_total	double precision;
	v_saldo_gudang	double precision;
	v_litem	int;
	v_lgudang int;
	
begin
for rcd in
	select h.id_barang, coalesce(i.plu,'') plu, coalesce(i.nama_barang, '') nama_barang, h.warehouseid id_gudang, 
	coalesce(g.nama_gudang,'') nama_gudang, h.txdate tanggal, h.txtype, coalesce(h.no_bukti,'') no_bukti, 
	h.description keterangan, coalesce(h.sellingprice,0) sellingprice, coalesce(h.item_cost,0) item_cost, 
	coalesce(h.quantity,0) quantity, h.itemhistid
	from item_history h
	inner join m_item i on i.id=h.id_barang
	left join m_gudang g on g.id=h.warehouseid
	where case when v_item='' then true else id_barang=v_item::int  end
	AND case when v_gudang='' then true else h.warehouseid=v_gudang::int end
	and h.txdate>=v_tanggal1::date
	and h.txdate<=v_tanggal2::date
	order by coalesce(i.nama_barang, ''), h.warehouseid, h.txdate, h.itemhistid
	
LOOP
	raise notice 'v_litem: %, itemskg: %, lgudang: %, gdgskg: %', v_litem, rcd.id_barang, v_lgudang, rcd.id_gudang;
	
	if(coalesce(v_litem,0)<>rcd.id_barang) then
		v_saldo_total=0;
		v_saldo_gudang=0;
		v_litem = rcd.id_barang;
	end if;
	if(coalesce(v_lgudang,0)<>rcd.id_gudang) then
		v_saldo_gudang=0;
		v_lgudang=rcd.id_gudang;
	end if;
	v_saldo_total=v_saldo_total+coalesce(rcd.quantity,0);
	v_saldo_gudang=v_saldo_gudang+coalesce(rcd.quantity,0);
	
	return QUERY select rcd.id_barang, rcd.plu, rcd.nama_barang, rcd.id_gudang, rcd.nama_gudang, 
	rcd.tanggal, rcd.txtype, rcd.no_bukti, rcd.keterangan, rcd.sellingprice, rcd.item_cost, rcd.quantity, 
	rcd.itemhistid, 
	case when rcd.quantity>0 then rcd.quantity else 0 end masuk, 
	case when rcd.quantity<0 then abs(rcd.quantity) else 0 end keluar, 
	v_saldo_gudang, v_saldo_total;
	
END LOOP;		
/*
select * from fn_rpt_kartu_stok_formula('', '', '2017-01-01', '2017-10-01') as (id_barang integer, plu varchar, nama_barang varchar, id_gudang integer,
nama_gudang varchar, tanggal date, trx_type varchar, no_bukti varchar, keterangan varchar, selling_price double precision, item_cost double precision,
quantity double precision, id bigint, masuk double precision, keluar double precision, saldo_gudang double precision, 
saldo_total double precision)

select id_barang, count(distiNct warehouseid), COUNT(*) from item_history
group by id_barang
having count(distiNct warehouseid) > 1
*/
end
$BODY$
  LANGUAGE plpgsql VOLATILE;
  
  
CREATE OR REPLACE FUNCTION public.fn_tg_ar_inv_det()
  RETURNS trigger AS
$BODY$
declare
	r_arinv		record;
	r_stok		record;
	v_priority 	int;
	v_qty_jual	double precision;
	v_qty_sisa	double precision;
	v_qty_pakai	double precision;
	v_tot_qty_so	double precision;
	v_tot_qty_jual	double precision;
	
begin
v_qty_jual:=0;
v_qty_sisa:=0;

if(TG_OP='INSERT' or TG_OP='UPDATE') then
	select into r_arinv i.*, t.keterangan, m.nama_relasi as nama_customer --t.kode=i.trx_type
	from ar_inv i
	left join m_trx_type t on t.kode=i.trx_type
	left join m_relasi m on m.id_relasi=i.id_customer
	where id=NEW.ar_id;

	select into v_tot_qty_jual sum(qty) from ar_inv_det d
	inner join ar_inv h on h.id=ar_id and h.no_so=r_arinv.no_so;

	select into v_tot_qty_so sum(qty) from so_Det where no_so=r_arinv.no_so;

	if(v_tot_qty_jual>=v_tot_qty_so) then
		update so set closed=true where no_so=r_arinv.no_so;
	end if;	
	
	if(exists(select * from m_item_price_by_customer where id_barang = NEW.id_barang and  id_customer=r_arinv.id_customer)) then
		UPDATE m_item_price_by_customer
		   SET price=NEW.unit_price, disc=NEW.disc, ppn=NEw.ppn, last_trx_id=NEW.ar_id
		WHERE id_barang = NEW.id_barang and  id_customer=r_arinv.id_customer;
	else
		INSERT INTO m_item_price_by_customer(
		id_barang, id_customer, price, disc, ppn, last_trx_id)
		VALUES (NEW.id_barang, r_arinv.id_customer, NEw.unit_price, NEW.disc, NEW.ppn, NEw.ar_id);
	end if;

	if r_arinv.id_sales is not null then
		if(exists(select * from m_item_price_by_salesman where id_barang = NEW.id_barang and  id_sales=r_arinv.id_sales)) then
			UPDATE m_item_price_by_salesman
			   SET price=NEW.unit_price, disc=NEW.disc, ppn=NEw.ppn, last_trx_id=NEW.ar_id
			WHERE id_barang = NEW.id_barang and  id_sales=r_arinv.id_sales;
		else
			INSERT INTO m_item_price_by_salesman(
			id_barang, id_sales, price, disc, ppn, last_trx_id)
			VALUES (NEW.id_barang, r_arinv.id_sales, NEw.unit_price, NEW.disc, NEW.ppn, NEw.ar_id);
		end if;
	end if;
	if(TG_OP='INSERT') then
		if r_arinv.trx_type='PJL' THEN
			v_qty_jual=coalesce(NEW.qty,0);
			v_qty_sisa=coalesce(NEW.qty,0);
			raise notice 'Qty: %', coalesce(NEW.qty,0);

			for r_stok in
				select * from item_history where id_barang=NEW.id_barang and qtycontrol>0 and warehouseid=r_arinv.id_gudang order by txdate, itemhistid
				
			LOOP
				v_qty_pakai:=case when r_stok.qtycontrol>=v_qty_sisa then v_qty_sisa else r_stok.qtycontrol end;
				raise notice 'Masih ada %, Pakai %', r_stok.qtycontrol, v_qty_sisa;

				INSERT INTO item_history(
					    id_barang, txdate, txtype, invoiceid, description, 
					    quantity, sellingprice, item_cost, costavr, prevcost, prevqty, 
					    glperiod, glyear, warehouseid, fifohistid, fifostart, qtycontrol, no_bukti, det_id)
				select new.id_barang, r_arinv.invoice_date, r_arinv.trx_type, r_arinv.id, r_arinv.keterangan||' '||r_arinv.nama_customer , 
					    -v_qty_pakai, (coalesce(NEW.unit_price,0)-case when isnumeric(NEW.disc) then NEW.disc::double precision else 0 end)*(1+coalesce(NEW.ppn,0)/100), r_stok.item_cost, 0, 0, v_qty_pakai,
					    to_char(r_arinv.invoice_date, 'MM')::smallint, to_char(r_arinv.invoice_date, 'YYYY')::int, r_arinv.id_gudang, r_stok.itemhistid, 0, 0, 
					    r_arinv.invoice_no, NEW.det_id
				from m_Item i where i.id=NeW.id_barang;

				update item_history set qtycontrol=r_stok.qtycontrol-v_qty_pakai
				where itemhistid=r_stok.itemhistid;

			
				v_qty_sisa=v_qty_sisa-v_qty_pakai;

				if(v_qty_sisa<=0) then
					exit;
				end if;
			END LOOP;	
			if(v_qty_sisa<>0) then
				raise notice 'Masih sisa: %', v_qty_sisa;

				INSERT INTO item_history(
					    id_barang, txdate, txtype, invoiceid, description, 
					    quantity, sellingprice, item_cost, costavr, prevcost, prevqty, 
					    glperiod, glyear, warehouseid, fifohistid, fifostart, qtycontrol, no_bukti, det_id)
				select new.id_barang, r_arinv.invoice_date, r_arinv.trx_type, r_arinv.id, r_arinv.keterangan||' '||r_arinv.nama_customer , 
					    -v_qty_sisa, (coalesce(NEW.unit_price,0)-case when isnumeric(NEW.disc) then NEW.disc::double precision else 0 end)*(1+coalesce(NEW.ppn,0)/100), 
					    coalesce(i.hpp,0) hpp, 0, 0, 0,
					    to_char(r_arinv.invoice_date, 'MM')::smallint, to_char(r_arinv.invoice_date, 'YYYY')::int, r_arinv.id_gudang, r_stok.itemhistid, 0, 0, 
					    r_arinv.invoice_no, NEW.det_id
				from m_Item i where i.id=NeW.id_barang;
			end if;
		END IF;
		IF r_arinv.trx_type='RJL' THEN
			v_qty_jual=abs(coalesce(NEW.qty,0));
			v_qty_sisa=abs(coalesce(NEW.qty,0));
			raise notice 'Qty Retur: %', coalesce(NEW.qty,0);
			
			for r_stok in
				select * from item_history where id_barang=NEW.id_barang and invoiceid=r_arinv.retur_dari 
				and prevqty>0
				order by itemhistid desc
			LOOP
				
				INSERT INTO item_history(
					    id_barang, txdate, txtype, invoiceid, description, 
					    quantity, sellingprice, item_cost, costavr, prevcost, prevqty, 
					    glperiod, glyear, warehouseid, fifohistid, fifostart, qtycontrol, no_bukti, det_id, retur_id)
				select new.id_barang, r_arinv.invoice_date, r_arinv.trx_type, r_arinv.id, r_arinv.keterangan||' - '||i.nama_barang , 
					    case when abs(r_stok.quantity)>v_qty_sisa then v_qty_sisa else -r_stok.quantity end, 
					    (coalesce(NEW.unit_price,0)-case when isnumeric(NEW.disc) then NEW.disc::double precision else 0 end)*(1+coalesce(NEW.ppn,0)/100), 
					    r_stok.item_cost, 0, 0, 0,
					    to_char(r_arinv.invoice_date, 'MM')::smallint, to_char(r_arinv.invoice_date, 'YYYY')::int, r_arinv.id_gudang, r_stok.fifohistid, 0, 0, 
					    r_arinv.invoice_no, NEW.det_id, r_stok.itemhistid
				from m_Item i where i.id=NeW.id_barang;

				update item_history set qtycontrol=qtycontrol+case when abs(r_stok.quantity)>v_qty_sisa then v_qty_sisa else abs(r_stok.quantity) end
				where itemhistid=r_stok.fifohistid;

				update item_history set prevqty=prevqty-case when abs(r_stok.quantity)>v_qty_sisa then v_qty_sisa else -r_stok.quantity end
				where itemhistid=r_stok.itemhistid;
				
				v_qty_sisa=v_qty_sisa+r_stok.quantity;

				if(v_qty_sisa<=0) then
					exit;
				end if;
			END LOOP;
			if(v_qty_sisa<>0) then
				raise notice 'Masih sisa: %', v_qty_sisa;

				INSERT INTO item_history(
					    id_barang, txdate, txtype, invoiceid, description, 
					    quantity, sellingprice, item_cost, costavr, prevcost, prevqty, 
					    glperiod, glyear, warehouseid, fifohistid, fifostart, qtycontrol, no_bukti, det_id)
				select new.id_barang, r_arinv.invoice_date, r_arinv.trx_type, r_arinv.id, r_arinv.keterangan||' '||r_arinv.nama_customer , 
					    v_qty_sisa, (coalesce(NEW.unit_price,0)-case when isnumeric(NEW.disc) then NEW.disc::double precision else 0 end)*(1+coalesce(NEW.ppn,0)/100), 
					    coalesce(i.hpp,0) hpp, 0, 0, 0,
					    to_char(r_arinv.invoice_date, 'MM')::smallint, to_char(r_arinv.invoice_date, 'YYYY')::int, r_arinv.id_gudang, r_stok.itemhistid, 0, 0, 
					    r_arinv.invoice_no, NEW.det_id
				from m_Item i where i.id=NeW.id_barang;
			end if;
		END IF;
		return NEW;
	end if;		
	if(TG_OP='UPDATE') then
		UPDATE item_history 
			set 	id_barang=new.id_barang, 
				txdate=r_arinv.invoice_date, 
				txtype=r_arinv.trx_type, 
				invoiceid=r_arinv.id, 
				description=r_arinv.keterangan||' '||i.nama_barang, 
				quantity=NEW.qty, 
				sellingprice=(coalesce(NEW.unit_price,0)-case when isnumeric(NEW.disc) then NEW.disc::double precision else 0 end)*(1+coalesce(NEW.ppn,0)/100), 
				item_cost=coalesce(i.hpp,0), 
				costavr=0, 
				prevcost=0, 
				prevqty=0, 
				glperiod=to_char(r_arinv.invoice_date, 'MM')::smallint, 
				glyear=to_char(r_arinv.invoice_date, 'YYYY')::int, 
				warehouseid=r_arinv.id_gudang, 
				fifohistid=null, 
				fifostart=New.qty, 
				qtycontrol=New.qty, 
				no_bukti=r_arinv.invoice_no
		from m_Item i where i.id=NeW.id_barang 
		and invoiceid=r_arinv.id and id_barang=new.id_barang and txtype=r_arinv.trx_type;

		return NEW;
	end if;

	RETURN NULL;
end if;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;

delete from item_history ;
alter SEQUENCE item_history_itemhistid_seq  restart with 1;

INSERT INTO item_history(
					    id_barang, txdate, txtype, invoiceid, description, 
					    quantity, sellingprice, item_cost, costavr, prevcost, prevqty, 
					    glperiod, glyear, warehouseid, fifohistid, fifostart, qtycontrol, no_bukti, det_id)

select *
from(
	select d.id_barang, h.invoice_date tanggal, h.trx_type, h.id, t.keterangan||' '||coalesce(r.nama_relasi,'') keterangan , 
	-d.qty as qty, (coalesce(d.unit_price,0)-case when isnumeric(d.disc) then d.disc::double precision else 0 end)*(1+coalesce(d.ppn,0)/100) sellingprice, 
	coalesce(i.hpp,0) hpp, 0, 0, 0,
	to_char(h.invoice_date, 'MM')::smallint, to_char(h.invoice_date, 'YYYY')::int, h.id_gudang, null, 0, 0, 
	h.invoice_no, d.det_id
	from m_Item i 
	inner join ar_inv_det d on d.id_barang=i.id
	inner join ar_inv h on h.id=d.ar_id
	left join m_trx_type t on t.kode=h.trx_type
	left join m_relasi r on r.id_relasi=h.id_customer
	--select count(*) from ar_inv_det  --13903

	UNION ALL

	select d.id_barang, h.tanggal, 'OPN' trx_type, h.id, 'SO - '||h.keterangan keterangan, 
	coalesce(d.qty_baru,0) - coalesce(d.qty_sekarang,0) qty, d.hpp_baru, d.hpp_baru, 0, 0, coalesce(d.qty_baru,0) - coalesce(d.qty_sekarang,0) prevqty,
	to_char(h.tanggal, 'MM')::smallint, to_char(h.tanggal, 'YYYY')::int, h.id_gudang, 0, 0, coalesce(d.qty_baru,0), 
	h.no_bukti, d.seq det_id
	from opname h
	inner join opname_detail d on d.id_opname=h.id

	UNION ALL
		
	select d.id_barang, h.invoice_date, h.trx_type, h.id, t.keterangan||' - '||coalesce(r.nama_relasi,'') as keterangan , 
	d.qty, 0, fn_get_Disc_Bertingkat(coalesce(d.unit_price,0), coalesce(d.disc,''))*(1+coalesce(d.ppn,0)/100), 0, 0, 0, 
	to_char(h.invoice_date, 'MM')::smallint, to_char(h.invoice_date, 'YYYY')::int, h.id_gudang, null, 
	d.qty, d.qty, h.invoice_no, d.det_id
	from m_Item i 
	inner join ap_inv_det d on d.id_barang=i.id
	inner join ap_inv h on h.id=d.ap_id
	inner join m_relasi r on r.id_relasi=h.supplier_id
	left join m_trx_type t on t.kode=h.trx_type
	--select count(*) from ap_inv_det --975
)x
order by case trx_type when 'OPN' then 1 when 'PBL' then 2 else 3 end, x.tanggal, x.det_id
