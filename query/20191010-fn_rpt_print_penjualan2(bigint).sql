﻿-- Function: public.fn_rpt_print_penjualan(bigint)

-- DROP FUNCTION public.fn_rpt_print_penjualan(bigint);

CREATE OR REPLACE FUNCTION public.fn_rpt_print_penjualan2(bigint)
  RETURNS SETOF record AS
$BODY$
declare
	v_id	alias for $1;
	rcd	record;
	v_nett	double precision;
	v_disc_rp double precision;
	v_ket_faktur text;
	v_jml_item int;
	v_empty_row	int;
	v_baris_per_page int;
begin

v_baris_per_page=12;

select into v_nett, v_disc_rp 
	fn_get_Disc_Bertingkat(sum(fn_get_Disc_Bertingkat(d.qty*coalesce(d.unit_price,0), coalesce(d.disc,''))*(1+coalesce(d.ppn,0)/100)), coalesce(h.ar_disc,''))+coalesce(h.biaya_lain,0), 
	sum(fn_get_Disc_Bertingkat(d.qty*coalesce(d.unit_price,0), coalesce(d.disc,''))*(1+coalesce(d.ppn,0)/100))-fn_get_Disc_Bertingkat(sum(fn_get_Disc_Bertingkat(d.qty*coalesce(d.unit_price,0), coalesce(d.disc,''))*(1+coalesce(d.ppn,0)/100)), coalesce(h.ar_disc,''))
from ar_inv  h
--select * from ar_inv where invoice_no='015906'
inner join ar_inv_det d on d.ar_id=h.id
where h.id=v_id
group by coalesce(h.ar_disc,''), coalesce(h.biaya_lain,0);

select into v_jml_item count(*) from ar_inv_det where ar_id=v_id;
v_empty_row = case when v_jml_item < v_baris_per_page then 8-v_jml_item else 8 - mod(v_jml_item, v_baris_per_page) end;

select into v_ket_faktur coalesce(ket_pembayaran,'') from m_setting_penjualan;

FOR rcd in
	select i.id, i.invoice_no as no_faktur, fn_get_tgl_ind(i.invoice_date::date) as tgl_faktur, coalesce(i.no_so, '') as no_so, coalesce(g.nama_gudang,'') as nama_gudang, 
	coalesce(c.nama_relasi,'') as nama_customer, coalesce(c.alamat,'') as alamat_cust, coalesce(k.nama_kota,'') as kota_customer, 
	case when coalesce(c.telp,'')<>'' then c.telp else coalesce(c.hp,'') end as telp_hp, coalesce(ex.nama_expedisi,'') as nama_expedisi,
	fn_get_tgl_ind(i.invoice_date+coalesce(i.top,0)) as jt_tempo, coalesce(i.description,'') as ket_faktur, 
	v_ket_faktur,  --coalesce(i.ket_pembayaran,'') as ket_pembayaran,
	case when i.jenis_bayar='K' then 'Kredit' else 'Tunai' end as status_bayar, coalesce(s.nama_sales,'') as nama_sales, 
	(coalesce(i.ar_disc,'')||case when coalesce(i.ar_disc,'')<>'' then '%' else '' end)::varchar as ar_disc, coalesce(v_disc_rp,0) as ar_disc_rp, coalesce(v_nett,0) as nett, 
	coalesce(m_item.plu,'') as plu, coalesce(m_item.nama_barang,'') as nama_barang, 
	d.qty, coalesce(m_item.satuan,'') as satuan, d.unit_price, d.disc, d.ppn, coalesce(d.biaya,0) as biaya,
	d.qty*fn_get_Disc_Bertingkat(coalesce(d.unit_price,0), coalesce(d.disc,''))*(1+coalesce(d.ppn,0)/100)+coalesce(d.biaya,0) as sub_total, coalesce(d.keterangan,'') as keterangan, 
	'Gudang'::varchar as disiapkan_oleh, --coalesce(i.disiapkan_oleh,'') as disiapkan_oleh, 
	coalesce(u.complete_name,'') as dibuat_oleh, coalesce(i.diperiksa_oleh,'') as diperiksa_oleh, coalesce(i.diterima_oleh,'') as diterima_oleh,
	coalesce(i.biaya_lain,0) as biaya_lain
	from ar_inv  i
	inner join ar_inv_det d on d.ar_id=i.id
	inner join m_item on m_item.id=d.id_barang
	left join m_relasi c on c.id_relasi=i.id_customer
	left join m_gudang g on g.id=i.id_gudang
	left join m_salesman s on s.id_sales=i.id_sales
	left join m_expedisi ex on ex.id=i.id_expedisi
	left join m_user u on upper(u.user_name)=upper(i.user_ins)
	left join m_kota k on k.id=c.id_kota
	where i.id=v_id
	order by i.id, d.seq

LOOP
	return next rcd;
END LOOP;

FOR i IN 1..v_empty_row LOOP
    return QUERY select null::bigint id, ''::varchar as no_faktur, ''::varchar as tgl_faktur, ''::varchar as no_so, ''::varchar as nama_gudang, 
	''::varchar as nama_customer, ''::varchar as alamat_cust, ''::varchar as kota_customer, 
	''::varchar as telp_hp, ''::varchar as nama_expedisi,
	''::varchar as jt_tempo, ''::text as ket_faktur, 
	v_ket_faktur,  
	''::text as status_bayar, coalesce(s.nama_sales,'') ::varchar as nama_sales, 
	''::varchar as ar_disc, coalesce(v_disc_rp,0) as ar_disc_rp, coalesce(v_nett,0) as nett, 
	''::varchar as plu, ''::varchar as nama_barang, 
	0::double precision qty, ''::varchar as satuan, 0::double precision unit_price, ''::varchar disc, 0::double precision ppn, 
	0::double precision as biaya, 0::double precision as sub_total, ''::varchar as keterangan, 
	'Gudang'::varchar as disiapkan_oleh, 
	coalesce(u.complete_name,'') as dibuat_oleh, coalesce(i.diperiksa_oleh,'') as diperiksa_oleh, coalesce(i.diterima_oleh,'') as diterima_oleh,
	0::double precision as biaya_lain
	from ar_inv  i
	left join m_user u on upper(u.user_name)=upper(i.user_ins)
	left join m_salesman s on s.id_sales=i.id_sales
	where i.id=v_id;
END LOOP;

/*
select * from fn_rpt_print_penjualan2(2) as (id bigint, no_faktur varchar, tgl_faktur varchar, no_so varchar, nama_gudang varchar, nama_customer varchar, 
alamat_customer varchar, kota_customer varchar, telp_hp varchar, nama_expedisi varchar, jt_tempo varchar, ket_faktur text, ket_pembayaran text, status_bayar text, 
nama_sales varchar, ar_disc varchar, ar_disc_rp double precision, nett double precision, plu varchar, nama_barang varchar, qty double precision, satuan varchar, 
unit_price double precision, disc varchar, ppn double precision, 
biaya double precision, sub_total double precision, keterangan varchar, disiapkan_oleh varchar, dibuat_oleh varchar, diperiksa_oleh varchar, diterima_oleh varchar, 
biaya_lain double precision)
*/
end
$BODY$
  LANGUAGE plpgsql VOLATILE;
