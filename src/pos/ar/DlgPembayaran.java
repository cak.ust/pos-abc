/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * DlgPembayaran.java
 *
 * Created on 07 Jan 11, 20:38:12
 */
package pos.ar;

import com.ustasoft.component.GeneralFunction;
import com.ustasoft.pos.service.PrintTrxService;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FocusTraversalPolicy;
import java.awt.GraphicsEnvironment;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author cak-ust
 */
public class DlgPembayaran extends javax.swing.JDialog {

    private double dBayar = 0, dDiskon = 0;
    private GeneralFunction fn = new GeneralFunction();
    private boolean isSelected = false;
    private String sNoTrx = "";
    private FrmPenjualan srcFormPenjualan;
    private Object srcForm;

    /**
     * Creates new form DlgPembayaran
     */
    public DlgPembayaran(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        MyKeyListener kListener = new MyKeyListener();
        fn.addKeyListenerInContainer(jPanel1, kListener, txtFocusListener);
        //get the size of the screen
        Dimension windowSize = getSize();
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        //Determine the new location of the window
        Point centerPoint = ge.getCenterPoint();
        System.out.println("windowSize: " + windowSize);
//        int dx = centerPoint.x - windowSize.width/2;
        int dx = centerPoint.x * 2 - this.getWidth();
        int dy = centerPoint.y - windowSize.height / 2;
        System.out.println("centerPoint: " + centerPoint);
        System.out.println("dx: " + dx);
        System.out.println("dy: " + dy);
        setLocation(dx, dy);

    }

    public boolean isSelected() {
        return isSelected;
    }

    public double getBayar() {
        return dBayar;
    }

    public double getDiskon() {
        return dDiskon;
    }

    private void hitung() {
        double total = fn.udfGetDouble(txtGrandTotal.getText()) - fn.udfGetDouble(txtDiskon.getText());
        double kredit = total - fn.udfGetDouble(txtTunai.getText()) > 0 ? total - fn.udfGetDouble(txtTunai.getText()) : 0;
        double kembali = fn.udfGetDouble(txtTunai.getText()) - total > 0 ? fn.udfGetDouble(txtTunai.getText()) - total : 0;
        txtKredit.setText(fn.intFmt.format(kredit));
        txtKembali.setText(fn.intFmt.format(Math.abs(kembali)));

    }

    public void setNoTrx(String text) {
        this.sNoTrx = text;
    }

    public void setTotal(double udfGetDouble) {
        txtGrandTotal.setText(fn.intFmt.format(udfGetDouble));
        txtTunai.setText(fn.intFmt.format(udfGetDouble));
    }

    public void setFormPenjualan(FrmPenjualan aThis) {
        this.srcFormPenjualan = aThis;
    }

    void setSrcForm(Object srcForm) {
        //terisi jika dobel buka form penjualan
        this.srcForm = srcForm;
    }

    public class MyKeyListener extends KeyAdapter {

        @Override
        public void keyReleased(KeyEvent evt) {
            if (evt.getSource().equals(txtTunai)) {
                txtTunai.setText(GeneralFunction.intFmt.format(GeneralFunction.udfGetDouble(txtTunai.getText())));
                hitung();
            }
        }

        @Override
        public void keyTyped(KeyEvent evt) {
            fn.keyTyped(evt);
        }

        @Override
        public void keyPressed(KeyEvent evt) {
            Component ct = KeyboardFocusManager.getCurrentKeyboardFocusManager().getPermanentFocusOwner();
            int keyKode = evt.getKeyCode();
            switch (keyKode) {
                case KeyEvent.VK_F5: {
                    udfSelected();
                    break;
                }
                case KeyEvent.VK_F9: {
//                    if(tblDetail.getRowCount()==0) return;
//                    ((DefaultTableModel)tblHeader.getModel()).addRow(new Object[]{
//                        tblHeader.getRowCount()+1, "T", 0
//                    });
//                    tblHeader.requestFocusInWindow();
//                    tblHeader.requestFocus();
//                    tblHeader.setRowSelectionInterval(tblHeader.getRowCount()-1, tblHeader.getRowCount()-1);
//                    tblHeader.changeSelection(tblHeader.getRowCount()-1, 1, false, false);
                    break;
                }
                case KeyEvent.VK_ENTER: {
                    if (!(ct instanceof JTable)) {
                        if (!fn.isListVisible()) {
//                            if(evt.getSource() instanceof JTextField && ((JTextField)evt.getSource()).getText()!=null
//                               && ((JTextField)evt.getSource()).getName().equalsIgnoreCase("textEditor")){
//                                if(table.getSelectedColumn()==0){
//                                    //table.setValueAt(((JTextField)evt.getSource()).getText(), table.getSelectedRow(), 0);
//                                    //table.changeSelection(table.getSelectedRow(), 2, false, false);
//                                    //table.setColumnSelectionInterval(2, 2);
//                                }
//                            }

                            Component c = findNextFocus();
                            if (c == null) {
                                return;
                            }
                            if (c.isEnabled()) {
                                c.requestFocus();
                            } else {
                                c = findNextFocus();
                                if (c != null) {
                                    c.requestFocus();
                                };
                            }
                        } else {
                            fn.lstRequestFocus();
                        }
                    } else {

                    }
                    break;
                }
                case KeyEvent.VK_DOWN: {
                    if (ct instanceof JTable) {
                        if (((JTable) ct).getSelectedRow() == 0) {
                            Component c = findNextFocus();
                            if (c == null) {
                                return;
                            }
                            if (c.isEnabled()) {
                                c.requestFocus();
                            } else {
                                c = findNextFocus();
                                if (c != null) {
                                    c.requestFocus();
                                };
                            }
                        }
                    } else {
                        if (!fn.isListVisible()) {
                            Component c = findNextFocus();
                            if (c == null) {
                                return;
                            }
                            if (c.isEnabled()) {
                                c.requestFocus();
                            } else {
                                c = findNextFocus();
                                if (c != null) {
                                    c.requestFocus();
                                };
                            }
                        } else {
                            fn.lstRequestFocus();
                        }
                        break;
                    }
                }

                case KeyEvent.VK_UP: {
                    if (ct instanceof JTable) {
                        if (((JTable) ct).getSelectedRow() == 0) {
                            Component c = findPrevFocus();
                            if (c == null) {
                                return;
                            }
                            if (c.isEnabled()) {
                                c.requestFocus();
                            } else {
                                c = findNextFocus();
                                if (c != null) {
                                    c.requestFocus();
                                };
                            }
                        }
                    } else {
                        Component c = findPrevFocus();
                        if (c == null) {
                            return;
                        }
                        if (c.isEnabled()) {
                            c.requestFocus();
                        } else {
                            c = findNextFocus();
                            if (c != null) {
                                c.requestFocus();
                            };
                        }
                    }
                    break;
                }
                case KeyEvent.VK_ESCAPE: {
                    if (sNoTrx.length() == 0) {
                        System.exit(0);
                    }
                    dispose();
                    break;
                }
            }
        }

//        @Override
//        public void keyReleased(KeyEvent evt){
//            if(evt.getSource().equals(txtDisc)||evt.getSource().equals(txtQty)||evt.getSource().equals(txtUnitPrice))
//                GeneralFunction.keyTyped(evt);
//        }
        public Component findNextFocus() {
            // Find focus owner
            Component c = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
            Container root = c == null ? null : c.getFocusCycleRootAncestor();

            if (root != null) {
                FocusTraversalPolicy policy = root.getFocusTraversalPolicy();
                Component nextFocus = policy.getComponentAfter(root, c);
                if (nextFocus == null) {
                    nextFocus = policy.getDefaultComponent(root);
                }
                return nextFocus;
            }
            return null;
        }

        public Component findPrevFocus() {
            // Find focus owner
            Component c = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
            Container root = c == null ? null : c.getFocusCycleRootAncestor();

            if (root != null) {
                FocusTraversalPolicy policy = root.getFocusTraversalPolicy();
                Component prevFocus = policy.getComponentBefore(root, c);
                if (prevFocus == null) {
                    prevFocus = policy.getDefaultComponent(root);
                }
                return prevFocus;
            }
            return null;
        }

    }
    PrintTrxService printTrxService = new PrintTrxService();

    private void udfSelected() {
        if(btnSimpan.getText().equalsIgnoreCase("Tutup")){
            srcFormPenjualan.udfClear();
            this.dispose();
            return;
        }
        this.isSelected = true;
        dBayar = fn.udfGetDouble(txtTunai.getText());
        dDiskon = fn.udfGetDouble(txtDiskon.getText());
        if (srcFormPenjualan != null) {
            Integer idPenjualan = srcFormPenjualan.udfSave(dBayar);
            if (idPenjualan > 0) {
                if (JOptionPane.showConfirmDialog(this, "Simpan transaksi sukses\nSelanjutnya akan dicetak?", "Cetak?", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE) == JOptionPane.YES_OPTION) {
                    printTrxService.cetakFakturPenjualan(idPenjualan, this);
                }
                btnSimpan.setText("Tutup");
                btnSimpan.requestFocusInWindow();
                if (srcForm != null && srcForm instanceof FrmPenjualan) {
                    ((FrmPenjualan) srcForm).dispose();
                } else {
                    srcFormPenjualan.udfClear();
                }
            }
        }
//        this.dispose();
    }

    private FocusListener txtFocusListener = new FocusListener() {
        public void focusGained(FocusEvent e) {
            if (e.getSource() instanceof JTextField || e.getSource() instanceof JFormattedTextField) {
                ((JTextField) e.getSource()).setBackground(Color.YELLOW);
                ((JTextField) e.getSource()).setSelectionStart(0);
                ((JTextField) e.getSource()).setSelectionEnd(((JTextField) e.getSource()).getText().length());
            }
        }

        public void focusLost(FocusEvent e) {
            if (e.getSource() instanceof JTextField || e.getSource() instanceof JFormattedTextField) {
                ((JTextField) e.getSource()).setBackground(Color.WHITE);
                ((JTextField) e.getSource()).setText(fn.intFmt.format(
                        fn.udfGetDouble(((JTextField) e.getSource()).getText())
                ));
            }
        }
    };

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        txtKembali = new javax.swing.JLabel();
        txtKredit = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtTunai = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtGrandTotal = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtDiskon = new javax.swing.JTextField();
        btnSimpan = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(1, 1, 1));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setName("jPanel1"); // NOI18N
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtKembali.setBackground(new java.awt.Color(0, 0, 102));
        txtKembali.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        txtKembali.setForeground(new java.awt.Color(255, 255, 255));
        txtKembali.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtKembali.setText("0,00");
        txtKembali.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtKembali.setName("txtKembali"); // NOI18N
        txtKembali.setOpaque(true);
        jPanel1.add(txtKembali, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 220, 250, 40));

        txtKredit.setBackground(new java.awt.Color(0, 255, 255));
        txtKredit.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        txtKredit.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtKredit.setText("0,00");
        txtKredit.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtKredit.setName("txtKredit"); // NOI18N
        txtKredit.setOpaque(true);
        jPanel1.add(txtKredit, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 180, 250, 40));

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Kredit");
        jLabel10.setName("jLabel10"); // NOI18N
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 180, 190, 40));

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Kembali");
        jLabel11.setName("jLabel11"); // NOI18N
        jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 220, 190, 40));

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Tunai");
        jLabel12.setName("jLabel12"); // NOI18N
        jPanel1.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 140, 160, 40));

        txtTunai.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        txtTunai.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtTunai.setText("0");
        txtTunai.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtTunai.setName("txtTunai"); // NOI18N
        jPanel1.add(txtTunai, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 140, 250, 40));

        jSeparator1.setName("jSeparator1"); // NOI18N
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 130, 420, 10));

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 0));
        jLabel5.setText("Tekan 'Esc' untuk batal pembayaran");
        jLabel5.setName("jLabel5"); // NOI18N
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 340, 260, 20));

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Grand Total");
        jLabel7.setName("jLabel7"); // NOI18N
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 110, 30));

        txtGrandTotal.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        txtGrandTotal.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtGrandTotal.setText("1000");
        txtGrandTotal.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtGrandTotal.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtGrandTotal.setEnabled(false);
        txtGrandTotal.setName("txtGrandTotal"); // NOI18N
        jPanel1.add(txtGrandTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 40, 250, 40));

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Diskon (Rp.)");
        jLabel8.setName("jLabel8"); // NOI18N
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, 110, 30));

        txtDiskon.setEditable(false);
        txtDiskon.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        txtDiskon.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtDiskon.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtDiskon.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtDiskon.setEnabled(false);
        txtDiskon.setName("txtDiskon"); // NOI18N
        jPanel1.add(txtDiskon, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 80, 250, 40));

        btnSimpan.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/disk.png"))); // NOI18N
        btnSimpan.setText("Simpan (F5)");
        btnSimpan.setName("btnSimpan"); // NOI18N
        btnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSimpanActionPerformed(evt);
            }
        });
        jPanel1.add(btnSimpan, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 290, 540, 50));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 0));
        jLabel6.setText("Pembayaran");
        jLabel6.setName("jLabel6"); // NOI18N
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 120, 90, 20));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 580, 380));

        setSize(new java.awt.Dimension(597, 423));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSimpanActionPerformed
        udfSelected();
    }//GEN-LAST:event_btnSimpanActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                txtTunai.requestFocus();
            }
        });
        hitung();
    }//GEN-LAST:event_formWindowOpened

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                DlgPembayaran dialog = new DlgPembayaran(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSimpan;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField txtDiskon;
    private javax.swing.JTextField txtGrandTotal;
    private javax.swing.JLabel txtKembali;
    private javax.swing.JLabel txtKredit;
    private javax.swing.JTextField txtTunai;
    // End of variables declaration//GEN-END:variables

}
