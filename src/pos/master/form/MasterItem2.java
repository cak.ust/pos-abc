/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pos.master.form;

import com.ustasoft.component.GeneralFunction;
import com.ustasoft.pos.dao.jdbc.AccCoaDao;
import com.ustasoft.pos.dao.jdbc.ItemDao;
import com.ustasoft.pos.dao.jdbc.ItemKategoriDao;
import com.ustasoft.pos.dao.jdbc.RelasiDao;
import com.ustasoft.pos.dao.jdbc.SatuanDao;
import com.ustasoft.pos.domain.Item;
import com.ustasoft.pos.domain.ItemCoa;
import com.ustasoft.pos.domain.ItemKategori;
import com.ustasoft.pos.domain.Relasi;
import com.ustasoft.pos.domain.Satuan;
import com.ustasoft.pos.domain.view.AccCoaView;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.FocusTraversalPolicy;
import java.awt.KeyboardFocusManager;
import java.awt.Toolkit;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import pos.MainForm;
import pos.master.list.FrmListItem;

/**
 *
 * @author cak-ust
 */
public class MasterItem2 extends javax.swing.JDialog {

    private Connection conn;
    private Object srcForm;
    private RelasiDao daoSupplier;
    private ItemDao daoItem;
    private SatuanDao daoSatuan;
    private ItemKategoriDao daoItemKategori;
    private AccCoaDao daoAcc;
    private String sOldKode = "";
    GeneralFunction fn = new GeneralFunction();
    List<ItemKategori> itemKategori;
    List<AccCoaView> akunPersediaan;
    List<AccCoaView> akunPembelian;
    List<AccCoaView> akunReturPembelian;
    List<AccCoaView> akunPenjualan;
    List<AccCoaView> akunReturPenjualan;
    List<AccCoaView> akunHpp;
    List<Relasi> supplier;
    List<Satuan> satuan;
    private JFileChooser m_chooser = new JFileChooser();
    
    private FileFilter fFilter = new FileFilter() {
        public boolean accept(File f) {
            return (f.getName().toLowerCase().endsWith(".jpg")) || (f.isDirectory());
        }

        public String getDescription() {
            return "Image files (*.jpg)";
        }
    };
    private String sFotoFile="";
    private FileInputStream fisFoto;
    
    private void PilihFoto() {
    setCursor(new Cursor(Cursor.WAIT_CURSOR));
    FileInputStream in = null;
    try {
      m_chooser.setDialogTitle("Pilih foto");
      m_chooser.setFileFilter(fFilter);
      if (this.m_chooser.showOpenDialog(this) != 0) {
        return;
      }
      File fChoosen = this.m_chooser.getSelectedFile();
      in = new FileInputStream(fChoosen);
      ImageIcon myIcon = new ImageIcon(this.m_chooser.getSelectedFile().toString());

        ImageIcon bigImage = new ImageIcon(myIcon.getImage().getScaledInstance(
                lblImage.getWidth(), 
                lblImage.getHeight(), 8));

        sFotoFile = this.m_chooser.getSelectedFile().toString();
        fisFoto = in;
        lblImage.setIcon(bigImage);
      setCursor(new Cursor(0));
      in.close();
    }
    catch (IOException ex) {
      setCursor(new Cursor(0));
      JOptionPane.showMessageDialog(this, ex.getMessage());
    } finally {
      try {
        setCursor(new Cursor(0));
        if (in != null) in.close(); 
      }
      catch (IOException ex) { 
          Logger.getLogger(MasterItem2.class.getName()).log(Level.SEVERE, null, ex); 
      }

    }
  }

    /**
     * Creates new form MasterCoa
     */
    public MasterItem2(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        tblHarga.getColumn("Mark Up (%)").setCellEditor(new MyTableCellEditor());
        tblHarga.getColumn("Harga Jual").setCellEditor(new MyTableCellEditor());
        tblHarga.setRowHeight(22);
        fn.addKeyListenerInContainer(jPanel2, new MyKeyListener(), txtFocusListener);

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                txtNama.requestFocusInWindow();
            }
        });
    }

    public void setSourceForm(Object obj) {
        srcForm = obj;

    }

    public void setConn(Connection con) {
        this.conn = con;
        daoSupplier = new RelasiDao(conn);
        udfInitForm();
    }

    private void udfClear() {
        lblID.setText("");
        txtNama.setText("");
        txtNama.setText("");
        txtBarcode.setText("");
        chkActive.setSelected(true);
    }

    private void udfInitForm() {
        try {
            daoItemKategori = new ItemKategoriDao(conn);
            daoItem = new ItemDao(conn);
            daoSatuan = new SatuanDao(conn);

            cmbKategori.removeAllItems();
            itemKategori = daoItemKategori.cariSemuaKategori();
            for (ItemKategori kat : itemKategori) {
                cmbKategori.addItem(kat.getNamaKategori());
            }

            cmbSatuan.removeAllItems();
            satuan = daoSatuan.cariSemuaData();
            for (Satuan sat : satuan) {
                cmbSatuan.addItem(sat.getSatuan());
            }

            cmbSupplier.removeAllItems();
            daoSupplier = new RelasiDao(conn);
            supplier = daoSupplier.cariSemuaData(1);
            for (Relasi supp : supplier) {
                cmbSupplier.addItem(supp.getNamaRelasi());
            }

            daoAcc = new AccCoaDao(conn);

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage());
        }
    }

    private boolean udfCekBeforeSave(){
        if (txtNama.getText().length() == 0) {
             JOptionPane.showMessageDialog(this, "Nama Stok harus diisi!", "Simpan Stok", JOptionPane.INFORMATION_MESSAGE);
             txtNama.requestFocus();
             return false;
         }
         if(cmbSatuan.getSelectedIndex()<0){
             JOptionPane.showMessageDialog(this, "Silahkan pilih satuan terlebih dulu!", "Simpan Stok", JOptionPane.INFORMATION_MESSAGE);
             cmbSatuan.requestFocus();
             return false;
         }
         if(txtPLU.getText().trim().length()>0){
             try{
                 ResultSet rs=conn.createStatement().executeQuery("select * from m_item where plu='"+txtPLU.getText()+"' and id<>"+
                         (lblID.getText().equalsIgnoreCase("")? "0": lblID.getText()));
                 if(rs.next()){
                     JOptionPane.showMessageDialog(this, "PLU tersebu sudah digunakan untuk item '"+rs.getString("nama_barang") +"'!", "Simpan Item", JOptionPane.INFORMATION_MESSAGE);
                     txtPLU.requestFocus();
                     rs.close();
                     return false;
                 }
                 rs.close();
             }catch(SQLException se){
                 JOptionPane.showMessageDialog(this, se.getMessage());
             }
         }
         return true;
    }
    
    private void udfSave() {
        try {
            btnSave.requestFocusInWindow();
            if(!udfCekBeforeSave())
                return;
            conn.setAutoCommit(false);
            Integer id = lblID.getText().trim().equalsIgnoreCase("") ? null : Integer.parseInt(lblID.getText());
            Integer idKota = cmbKategori.getSelectedIndex() < 0 ? null : itemKategori.get(cmbKategori.getSelectedIndex()).getId();
            Integer idTipeSupp = cmbSupplier.getSelectedIndex() < 0 ? null : supplier.get(cmbSupplier.getSelectedIndex()).getIdRelasi();
            Item item = new Item();
            item.setId(id);
            item.setPlu(txtPLU.getText());
            item.setBarcode(txtBarcode.getText());
            item.setNamaBarang(txtNama.getText());
            item.setId_kategori(itemKategori.get(cmbKategori.getSelectedIndex()).getId());
            item.setSatuan(cmbSatuan.getSelectedItem().toString());
            item.setIdSatuan(satuan.get(cmbSatuan.getSelectedIndex()).getId());
            item.setId_supp_default(supplier.get(cmbSupplier.getSelectedIndex()).getIdRelasi());
            item.setActive(chkActive.isSelected());
//            item.setHargaJual(fn.udfGetDouble(txtHarga.getText()));
            item.setTipe(cmbTipe.getSelectedIndex()==2? "S": cmbTipe.getSelectedItem().toString().substring(0, 1));
            item.setReorder(fn.udfGetInt(txtReorder.getText()));
            daoItem.simpanItem(item, null, lblID.getText().equalsIgnoreCase(""));
            
            daoItem.saveImage(item.getId(), sFotoFile, lblImage.getIcon());
            String ins="";
            for(int i=0; i<tblHarga.getRowCount(); i++){
                ins+=ins.equalsIgnoreCase("")? "":" union all \n";
                ins+="select fn_m_item_update_harga_jual("
                        +item.getId()+","
                        +fn.udfGetInt(tblHarga.getValueAt(i, tblHarga.getColumnModel().getColumnIndex("IdTipe")))+","
                        +fn.udfGetDouble(tblHarga.getValueAt(i, tblHarga.getColumnModel().getColumnIndex("Mark Up (%)")))+","
                        +fn.udfGetDouble(tblHarga.getValueAt(i, tblHarga.getColumnModel().getColumnIndex("Harga Jual")))+")";
            }
            System.out.println(ins);
            ResultSet rs=conn.createStatement().executeQuery(ins);
            if(rs.next()){
                System.out.println("Sukses update harga");
            }
            rs.close();
            rs=null;
            conn.setAutoCommit(true);
            if (srcForm instanceof FrmListItem) {
                ((FrmListItem) srcForm).tampilkanData(item.getId(), "");
            }
            JOptionPane.showMessageDialog(this, "Simpan Item sukses!");
            this.dispose();
        } catch (SQLException ex) {
            try {
                conn.rollback();
                conn.setAutoCommit(true);
                JOptionPane.showMessageDialog(this, ex.getMessage());
            } catch (SQLException ex1) {
                Logger.getLogger(MasterItem2.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }

    }
    private FocusListener txtFocusListener = new FocusListener() {
        public void focusGained(FocusEvent e) {
            if (e.getSource() instanceof JTextField || e.getSource() instanceof JFormattedTextField) {
                ((JTextField) e.getSource()).setBackground(Color.YELLOW);
                if ((e.getSource() instanceof JTextField && ((JTextField) e.getSource()).getName() != null && ((JTextField) e.getSource()).getName().equalsIgnoreCase("textEditor"))) {
                    ((JTextField) e.getSource()).setSelectionStart(0);
                    ((JTextField) e.getSource()).setSelectionEnd(((JTextField) e.getSource()).getText().length());

                }
            }
        }

        public void focusLost(FocusEvent e) {
            if (e.getSource().getClass().getSimpleName().equalsIgnoreCase("JTextField")
                    || e.getSource().getClass().getSimpleName().equalsIgnoreCase("JFormattedTextField")) {
                ((JTextField) e.getSource()).setBackground(Color.WHITE);
//                if(e.getSource().equals(txtHarga)){
//                    txtHarga.setText(GeneralFunction.intFmt.format(GeneralFunction.udfGetDouble(txtHarga.getText())));
//                }
            }
        }
    };

    private void settingCoa() {
        try {
            if (itemKategori == null || itemKategori.size() == 0 || !this.isShowing()) {
                return;
            }
            Integer id = itemKategori.get(cmbKategori.getSelectedIndex()).getId();
            ItemKategori kat = daoItemKategori.cariKategoriByID(id);
        } catch (Exception ex) {
            Logger.getLogger(MasterItem2.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public class MyKeyListener extends KeyAdapter {

        @Override
        public void keyTyped(java.awt.event.KeyEvent evt) {

//            if(getTableSource()==null)
//                return;

            if (evt.getSource() instanceof JTextField
                    && ((JTextField) evt.getSource()).getName() != null
                    && ((JTextField) evt.getSource()).getName().equalsIgnoreCase("textEditor")) {
                fn.keyTyped(evt);

            }

        }

        public void keyPressed(KeyEvent evt) {
            Component ct = KeyboardFocusManager.getCurrentKeyboardFocusManager().getPermanentFocusOwner();
            int keyKode = evt.getKeyCode();
            switch (keyKode) {
                case KeyEvent.VK_ENTER: {
                    if (!(ct instanceof JTable)) {
                        if (!fn.isListVisible()) {
                            Component c = findNextFocus();
                            if (c == null) {
                                return;
                            }
                            c.requestFocus();
                        } else {
                            fn.lstRequestFocus();
                        }
                    }
                    break;
                }
                case KeyEvent.VK_DOWN: {
                    if (!(ct.getClass().getSimpleName().equalsIgnoreCase("JTABLE"))) {
                        if (!fn.isListVisible()) {
                            Component c = findNextFocus();
                            if (c == null) {
                                return;
                            }
                            c.requestFocus();
                        } else {
                            fn.lstRequestFocus();
                        }
                        break;
                    }
                }

                case KeyEvent.VK_UP: {
                    if (!(evt.getSource() instanceof JTable)) {
                        Component c = findPrevFocus();
                        c.requestFocus();
                    }
                    break;
                }
                case KeyEvent.VK_F2: {
                    udfClear();
                    break;
                }
                case KeyEvent.VK_F5: {
                    udfSave();

                    break;
                }
                case KeyEvent.VK_ESCAPE: {
                    dispose();
                    break;
                }

                case KeyEvent.VK_DELETE: {

                    break;

                }

            }
        }

//        @Override
//        public void keyReleased(KeyEvent evt){
//            if(evt.getSource().equals(txtDisc)||evt.getSource().equals(txtQty)||evt.getSource().equals(txtUnitPrice))
//                GeneralFunction.keyTyped(evt);
//        }
        public Component findNextFocus() {
            // Find focus owner
            Component c = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
            Container root = c == null ? null : c.getFocusCycleRootAncestor();

            if (root != null) {
                FocusTraversalPolicy policy = root.getFocusTraversalPolicy();
                Component nextFocus = policy.getComponentAfter(root, c);
                if (nextFocus == null) {
                    nextFocus = policy.getDefaultComponent(root);
                }
                return nextFocus;
            }
            return null;
        }

        public Component findPrevFocus() {
            // Find focus owner
            Component c = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
            Container root = c == null ? null : c.getFocusCycleRootAncestor();

            if (root != null) {
                FocusTraversalPolicy policy = root.getFocusTraversalPolicy();
                Component prevFocus = policy.getComponentBefore(root, c);
                if (prevFocus == null) {
                    prevFocus = policy.getDefaultComponent(root);
                }
                return prevFocus;
            }
            return null;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        txtNama = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        lblID = new javax.swing.JTextField();
        chkActive = new javax.swing.JCheckBox();
        cmbKategori = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        cmbSupplier = new javax.swing.JComboBox();
        txtKeterangan = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        txtBarcode = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblHarga = new org.jdesktop.swingx.JXTable();
        jLabel9 = new javax.swing.JLabel();
        txtTglBeliTerakhir = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtHargaBeliTerakhir = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtStok = new javax.swing.JTextField();
        txtHppRata = new javax.swing.JTextField();
        jXHyperlink1 = new org.jdesktop.swingx.JXHyperlink();
        jXHyperlink2 = new org.jdesktop.swingx.JXHyperlink();
        jLabel8 = new javax.swing.JLabel();
        txtPLU = new javax.swing.JTextField();
        cmbSatuan = new javax.swing.JComboBox();
        jLabel22 = new javax.swing.JLabel();
        cmbTipe = new javax.swing.JComboBox();
        txtReorder = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btnNew = new javax.swing.JButton();
        btnClose = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        lblImage = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Master Stok");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel4.setText("Kategori :");
        jPanel2.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 85, 90, 20));

        txtNama.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtNama.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNamaActionPerformed(evt);
            }
        });
        jPanel2.add(txtNama, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 35, 320, 22));

        jLabel5.setText("ID :");
        jPanel2.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 50, 20));

        lblID.setEditable(false);
        lblID.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel2.add(lblID, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 10, 80, 22));

        chkActive.setSelected(true);
        chkActive.setText("Active");
        jPanel2.add(chkActive, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 10, 80, -1));

        cmbKategori.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbKategoriItemStateChanged(evt);
            }
        });
        jPanel2.add(cmbKategori, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 85, 270, -1));

        jLabel6.setText("Nama Stok");
        jPanel2.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 35, 90, 20));

        jLabel7.setText("Satuan");
        jPanel2.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 90, 20));

        jLabel15.setText("Supplier Utama :");
        jPanel2.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, -1, 20));

        jPanel2.add(cmbSupplier, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 110, 310, -1));

        txtKeterangan.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtKeterangan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtKeteranganActionPerformed(evt);
            }
        });
        jPanel2.add(txtKeterangan, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 185, 350, 22));

        jLabel17.setText("Barcode");
        jPanel2.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 60, 90, 20));

        txtBarcode.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtBarcode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBarcodeActionPerformed(evt);
            }
        });
        jPanel2.add(txtBarcode, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 60, 130, 22));

        jLabel18.setText("Keterangan :");
        jPanel2.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 185, 90, 20));

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Harga Penjualan", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tblHarga.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "IdTipe", "Tipe", "Mark Up (%)", "Harga Jual"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Double.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblHarga);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, 440, 90));

        jLabel9.setText("Tgl Beli Terakhir");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 110, 20));

        txtTglBeliTerakhir.setEditable(false);
        txtTglBeliTerakhir.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtTglBeliTerakhir.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(txtTglBeliTerakhir, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 20, 110, 22));

        jLabel10.setText("Hrg Beli Terakhir");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 45, 110, 20));

        txtHargaBeliTerakhir.setEditable(false);
        txtHargaBeliTerakhir.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtHargaBeliTerakhir.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(txtHargaBeliTerakhir, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 45, 110, 22));

        jLabel11.setText("Hpp Rata");
        jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 45, 80, 20));

        jLabel12.setText("Stok");
        jPanel1.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 20, 80, 20));

        txtStok.setEditable(false);
        txtStok.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtStok.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(txtStok, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 20, 110, 22));

        txtHppRata.setEditable(false);
        txtHppRata.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtHppRata.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(txtHppRata, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 45, 110, 22));

        jXHyperlink1.setText("Sesuaikan Harga Jual");
        jXHyperlink1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jXHyperlink1ActionPerformed(evt);
            }
        });
        jPanel1.add(jXHyperlink1, new org.netbeans.lib.awtextra.AbsoluteConstraints(77, 70, 150, -1));

        jXHyperlink2.setText("Sesuaikan Harga Jual");
        jXHyperlink2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jXHyperlink2ActionPerformed(evt);
            }
        });
        jPanel1.add(jXHyperlink2, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 70, 130, -1));

        jPanel2.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 215, 460, 190));

        jLabel8.setText("Kode :");
        jPanel2.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 10, 50, 20));

        txtPLU.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtPLU.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPLUActionPerformed(evt);
            }
        });
        jPanel2.add(txtPLU, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 10, 110, 22));

        cmbSatuan.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbSatuanItemStateChanged(evt);
            }
        });
        jPanel2.add(cmbSatuan, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 60, 110, 20));

        jLabel22.setText("Tipe :");
        jPanel2.add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 135, 60, 20));

        cmbTipe.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Inventory", "Non Inventory", "Jasa" }));
        jPanel2.add(cmbTipe, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 135, 140, -1));

        txtReorder.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtReorder.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtReorder.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtReorder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtReorderActionPerformed(evt);
            }
        });
        txtReorder.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtReorderKeyTyped(evt);
            }
        });
        jPanel2.add(txtReorder, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 160, 130, 22));

        jLabel23.setText("Reoder Level : ");
        jPanel2.add(jLabel23, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 160, 110, 20));

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, 470, 410));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel2.setText("Stok");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 440, 30));

        btnNew.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/application_lightning.png"))); // NOI18N
        btnNew.setText("Bersihkan (F2)");
        btnNew.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btnNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewActionPerformed(evt);
            }
        });
        getContentPane().add(btnNew, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 455, 110, -1));

        btnClose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/cancel.png"))); // NOI18N
        btnClose.setText("Tutup (Esc)");
        btnClose.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCloseActionPerformed(evt);
            }
        });
        getContentPane().add(btnClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 455, -1, -1));

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/disk.png"))); // NOI18N
        btnSave.setText("Simpan (F5)");
        btnSave.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        getContentPane().add(btnSave, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 455, 120, -1));

        lblImage.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblImage.setText("Gambar");
        lblImage.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lblImage.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblImageMouseClicked(evt);
            }
        });
        getContentPane().add(lblImage, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 40, 360, 380));

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/shape_move_front.png"))); // NOI18N
        jButton2.setText("Pilih Gambar");
        jButton2.setMargin(new java.awt.Insets(2, 2, 2, 2));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 420, 200, -1));

        jButton3.setText("Bersihkan gambar");
        jButton3.setMargin(new java.awt.Insets(2, 2, 2, 2));
        jButton3.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jButton3ItemStateChanged(evt);
            }
        });
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 420, 150, -1));

        setSize(new java.awt.Dimension(875, 524));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewActionPerformed
        udfClear();
    }//GEN-LAST:event_btnNewActionPerformed

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnCloseActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        udfSave();
    }//GEN-LAST:event_btnSaveActionPerformed

    private void txtNamaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNamaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNamaActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
    }//GEN-LAST:event_formWindowOpened

    private void txtKeteranganActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtKeteranganActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtKeteranganActionPerformed

    private void txtBarcodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBarcodeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtBarcodeActionPerformed

    private void cmbKategoriItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbKategoriItemStateChanged
        
    }//GEN-LAST:event_cmbKategoriItemStateChanged

    private void txtPLUActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPLUActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPLUActionPerformed

    private void cmbSatuanItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbSatuanItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbSatuanItemStateChanged

    private void lblImageMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblImageMouseClicked
        PilihFoto();
    }//GEN-LAST:event_lblImageMouseClicked

    private void jButton3ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jButton3ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton3ItemStateChanged

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        PilihFoto();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        lblImage.setIcon(null);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void txtReorderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtReorderActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtReorderActionPerformed

    private void txtReorderKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtReorderKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtReorderKeyTyped

    private void jXHyperlink2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jXHyperlink2ActionPerformed
        int colHarga = tblHarga.getColumnModel().getColumnIndex("Harga Jual");
        int colMarkup = tblHarga.getColumnModel().getColumnIndex("Mark Up (%)");
        double margin =0, hpp = fn.udfGetDouble(txtHppRata.getText());
        for(int i=0; i<tblHarga.getRowCount(); i++){
            margin = fn.udfGetDouble(tblHarga.getValueAt(i, colMarkup));
            tblHarga.setValueAt(hpp*(1+margin/100), i, colHarga);
        }
    }//GEN-LAST:event_jXHyperlink2ActionPerformed

    private void jXHyperlink1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jXHyperlink1ActionPerformed
        int colHarga = tblHarga.getColumnModel().getColumnIndex("Harga Jual");
        int colMarkup = tblHarga.getColumnModel().getColumnIndex("Mark Up (%)");
        double margin =0, hargaTerakhir = fn.udfGetDouble(txtHargaBeliTerakhir.getText());
        for(int i=0; i<tblHarga.getRowCount(); i++){
            margin = fn.udfGetDouble(tblHarga.getValueAt(i, colMarkup));
            tblHarga.setValueAt(hargaTerakhir*(1+margin/100), i, colHarga);
        }
    }//GEN-LAST:event_jXHyperlink1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MasterItem2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MasterItem2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MasterItem2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MasterItem2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                MasterItem2 dialog = new MasterItem2(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnNew;
    private javax.swing.JButton btnSave;
    private javax.swing.JCheckBox chkActive;
    private javax.swing.JComboBox cmbKategori;
    private javax.swing.JComboBox cmbSatuan;
    private javax.swing.JComboBox cmbSupplier;
    private javax.swing.JComboBox cmbTipe;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private org.jdesktop.swingx.JXHyperlink jXHyperlink1;
    private org.jdesktop.swingx.JXHyperlink jXHyperlink2;
    private javax.swing.JTextField lblID;
    private javax.swing.JLabel lblImage;
    private org.jdesktop.swingx.JXTable tblHarga;
    private javax.swing.JTextField txtBarcode;
    private javax.swing.JTextField txtHargaBeliTerakhir;
    private javax.swing.JTextField txtHppRata;
    private javax.swing.JTextField txtKeterangan;
    private javax.swing.JTextField txtNama;
    private javax.swing.JTextField txtPLU;
    private javax.swing.JTextField txtReorder;
    private javax.swing.JTextField txtStok;
    private javax.swing.JTextField txtTglBeliTerakhir;
    // End of variables declaration//GEN-END:variables

    public void tampilkanItem(Integer id) {
        try {
            Item view = daoItem.cariItemByID(id);
            lblID.setText(view.getId().toString());
            txtNama.setText(view.getNama_barang());
            txtPLU.setText(view.getPlu());
            cmbSatuan.setSelectedItem(daoSatuan.cariSatuanByID(view.getIdSatuan()).getSatuan());
            txtBarcode.setText(view.getBarcode());
            cmbKategori.setSelectedItem(daoItemKategori.cariKategoriByID(view.getId_kategori()).getNamaKategori());
            chkActive.setSelected(view.getActive());
//            txtHarga.setText(fn.intFmt.format(view.getHargaJual()));
            txtKeterangan.setText(view.getKeterangan());
            cmbTipe.setSelectedIndex(view.getTipe().equalsIgnoreCase("S")? 2: (view.getTipe().equalsIgnoreCase("N")? 1: 0));
            txtReorder.setText(fn.intFmt.format(view.getReorder()));
            if (view.getId_supp_default() != null) {
                cmbSupplier.setSelectedItem(daoSupplier.cariByID(view.getId_supp_default()).getNamaRelasi());
            }
            ItemCoa coa = daoItem.cariItemCoaByID(id);
            byte[] imgBytes=daoItem.getGambar(view.getId());
            if(imgBytes!=null){
                ImageIcon myIcon = new ImageIcon(imgBytes);
                ImageIcon bigImage = new ImageIcon(myIcon.getImage().getScaledInstance(360, 360, 8));

                lblImage.setIcon(bigImage);
                
            }
            //tampilkan harga beli terakhir
            String query="select * from fn_m_item_info_harga_stok("+id+") as (tgl date, harga_beli numeric, stok numeric, hpp_rata numeric)";
            System.out.println(query);
            ResultSet rs=MainForm.conn.createStatement().executeQuery(query);
            if(rs.next()){
                if(rs.getDate("tgl")!=null)
                    txtTglBeliTerakhir.setText(new SimpleDateFormat("dd/MM/yyyy").format(rs.getDate("tgl")));
                
                txtHargaBeliTerakhir.setText(fn.dFmt.format(rs.getDouble("harga_beli")));
                txtStok.setText(fn.dFmt.format(rs.getDouble("stok")));
                txtHppRata.setText(fn.dFmt.format(rs.getDouble("hpp_rata")));
            }
            rs.close();
            rs=null;
            //tampilkan margin
            query="select t.id, t.nama, coalesce(h.margin, coalesce(t.margin_default,0)) margin, coalesce(h.harga_jual,0) harga\n" +
                    "from m_tipe_harga_jual t \n" +
                    "left join m_item_harga_jual h on h.id_tipe_harga_jual=t.id and h.id_item="+id+"\n" +
                    "order by t.nama";
            System.out.println(query);
            ((DefaultTableModel)tblHarga.getModel()).setNumRows(0);
            rs=MainForm.conn.createStatement().executeQuery(query);
            while(rs.next()){
                ((DefaultTableModel)tblHarga.getModel()).addRow(new Object[]{
                    rs.getInt("id"),
                    rs.getString("nama"),
                    rs.getDouble("margin"),
                    rs.getDouble("harga"),
                });
            }
            rs.close();
            rs=null;
        } catch (Exception ex) {
            Logger.getLogger(MasterItem2.class.getName()).log(Level.SEVERE, null, ex);

        }

    }
    
    public class MyTableCellEditor extends AbstractCellEditor implements TableCellEditor {
        private Toolkit toolkit;
        JTextField text=new JTextField("");

        JLabel label =new JLabel("");

        int col, row;

        private NumberFormat  nf=NumberFormat.getInstance();

        public Component getTableCellEditorComponent(JTable table, Object value,
            boolean isSelected, int rowIndex, int vColIndex) {
            // 'value' is value contained in the cell located at (rowIndex, vColIndex)

//           label.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
//                public void propertyChange(java.beans.PropertyChangeEvent evt) {
//                    myModel.setValueAt(label.getText(), row, 1);
//                }
//            });

           col=vColIndex;
           row=rowIndex;
           text.setBackground(new Color(0,255,204));
//           if(col==0||col==2||col==3||col==5||col==6){
                text.addKeyListener(new java.awt.event.KeyAdapter() {
                    @Override
                   public void keyTyped(java.awt.event.KeyEvent evt) {
                      if (col!=0) {
                          char c = evt.getKeyChar();
                          if (!((c >= '0' && c <= '9') || c=='.') &&
                                (c != KeyEvent.VK_BACK_SPACE) &&
                                (c != KeyEvent.VK_DELETE) &&
                                (c != KeyEvent.VK_ENTER)) {
                                getToolkit().beep();
                                evt.consume();
                                return;
                          }
                       }
                    }
                    @Override
                   public void keyPressed(java.awt.event.KeyEvent evt){
                        if (fn.isListVisible() && evt.getKeyCode()==KeyEvent.VK_DOWN) {
                            fn.lstRequestFocus();
                        }
                   }
                    @Override
                   public void keyReleased(java.awt.event.KeyEvent evt) {

                   }
                });
//               }
//           }


           //component.selectAll();
           if (isSelected) {

           }
           //System.out.println("Value dari editor :"+value);
            text.setText(value==null? "": value.toString());
            //component.setText(df.format(value));

            if(value instanceof Double || value instanceof Float|| value instanceof Integer){
                try {
                    //Double dVal=Double.parseDouble(value.toString().replace(",",""));
                    Number dVal = nf.parse(value.toString());
                    text.setText(nf.format(dVal));


                } catch (java.text.ParseException ex) {
                    //Logger.getLogger(DlgLookupBarang.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else
                text.setText(value==null? "":value.toString());
           return text;
        }

        public Object getCellEditorValue() {
            Object o="";//=component.getText();
            Object retVal = 0;
                try {
                    //retVal = Integer.parseInt(((JTextField)component).getText().replace(",",""));
                    retVal = GeneralFunction.udfGetDouble(((JTextField)text).getText());
//                    o=nf.format(retVal);

//                    if((col==2||col==3) && (Double)retVal>0) tblAccount.setValueAt(0, row, (col==2? 3:2));

//                    //udfSetSubTotal(row);
//                    myModel.setValueAt( GeneralFunction.udfGetDouble(((JTextField)component).getText()) *
//                        GeneralFunction.udfGetDouble(myModel.getValueAt(row, tblAccount.getColumnModel().getColumnIndex("Harga")).toString()),
//                        row, tblAccount.getColumnModel().getColumnIndex("Sub Total"));

//                    return o;
                } catch (Exception e) {
                    toolkit.beep();
                    retVal=0;
                }
            return retVal;
        }

    }
}
