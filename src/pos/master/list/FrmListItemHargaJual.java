/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pos.master.list;

import com.ustasoft.component.GeneralFunction;
import com.ustasoft.component.MenuAuth;
import com.ustasoft.component.MyRowRenderer;
import com.ustasoft.pos.dao.jdbc.ItemDao;
import com.ustasoft.pos.dao.jdbc.ItemKategoriDao;
import com.ustasoft.pos.dao.jdbc.MenuAuthDao;
import com.ustasoft.pos.dao.jdbc.RelasiDao;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.FocusTraversalPolicy;
import java.awt.Font;
import java.awt.KeyboardFocusManager;
import java.awt.Toolkit;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractCellEditor;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.text.JTextComponent;
import org.jdesktop.swingx.JXDatePicker;
import pos.MainForm;
import pos.master.form.MasterItem;
import pos.master.form.MasterItem2;

/**
 *
 * @author cak-ust
 */
public class FrmListItemHargaJual extends javax.swing.JInternalFrame {

    private Connection conn;
    ItemDao itemDao;
    ItemKategoriDao itemKategoriDao;
    RelasiDao supplierDao;
    private GeneralFunction fn = new GeneralFunction();
    private final TableModelListener myTableModelListener = new TableModelListener() {
        public void tableChanged(TableModelEvent e) {
            int iRow = jTable1.getSelectedRow();
            if (iRow < 0 && jTable2.getSelectedRow() <= 0) {
                return;
            }
            ((DefaultTableModel) jTable2.getModel()).removeTableModelListener(this);
            System.out.println("Model Listener");
            double hargaBeliTerakhir = fn.udfGetDouble(txtHargaBeliTerakhir2.getText()) > 0 ? fn.udfGetDouble(txtHargaBeliTerakhir2.getText()) : fn.udfGetDouble(txtHppRata2.getText());
            if (e.getType() == TableModelEvent.UPDATE && e.getColumn() == jTable2.getColumnModel().getColumnIndex("Mark Up (%)")) {
                ((DefaultTableModel) jTable2.getModel()).setValueAt(fn.roundUp(hargaBeliTerakhir * (1 + fn.udfGetDouble(jTable2.getValueAt(jTable2.getSelectedRow(), 2)) / 100), MainForm.roundUp), jTable2.getSelectedRow(), 3);
            }
            if (e.getType() == TableModelEvent.UPDATE && e.getColumn() == jTable2.getColumnModel().getColumnIndex("Harga Jual")) {
                ((DefaultTableModel) jTable2.getModel()).setValueAt((fn.udfGetDouble(jTable2.getValueAt(jTable2.getSelectedRow(), 3)) - hargaBeliTerakhir) / hargaBeliTerakhir * 100, jTable2.getSelectedRow(), 2);
            }
            ((DefaultTableModel) jTable2.getModel()).addTableModelListener(this);
        }
    };

    /**
     * Creates new form ItemKategori
     */
    public FrmListItemHargaJual() {
        initComponents();
        jTable2.getColumn("Mark Up (%)").setCellEditor(new FrmListItemHargaJual.MyTableCellEditor());
        jTable2.getColumn("Harga Jual").setCellEditor(new FrmListItemHargaJual.MyTableCellEditor());
        jTable2.setRowHeight(22);
        jTable1.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                int iRow = jTable1.getSelectedRow();
                if (iRow < 0) {
                    return;
                }
                if (iRow >= 0) {
                    Integer id = Integer.valueOf(jTable1.getValueAt(iRow, jTable1.getColumnModel().getColumnIndex("ID")).toString());
                    btnNew.setEnabled(iRow >= 0 && menuAuth != null && menuAuth.canInsert());
                    btnEdit.setEnabled(iRow >= 0 && menuAuth != null && menuAuth.canUpdate());
                    btnDelete.setEnabled(iRow >= 0 && menuAuth != null && menuAuth.canDelete());
                    tampilkanHargaJual(id);
                }
            }
        });
        jTable2.getModel().addTableModelListener(myTableModelListener);
    }

    MenuAuth menuAuth;

    public void initForm(Connection con) {
        this.conn = con;
        for (int i = 0; i < jTable1.getColumnCount(); i++) {
            jTable1.getColumnModel().getColumn(i).setCellRenderer(new MyRowRenderer());
        }

        MyKeyListener kListener = new MyKeyListener();
        jTable1.addKeyListener(kListener);
        fn.addKeyListenerInContainer(jPanel1, kListener, null);
        txtCari.addKeyListener(kListener);
        btnRefresh.addKeyListener(kListener);

        itemDao = new ItemDao(conn);
        itemKategoriDao = new ItemKategoriDao(conn);
        supplierDao = new RelasiDao(conn);
        tampilkanData(0, "");

        menuAuth = new MenuAuthDao(conn).getMenuByUsername("Master Stok", MainForm.sUserName);
        if (menuAuth != null) {
            btnNew.setEnabled(menuAuth.canInsert());
            btnEdit.setEnabled(menuAuth.canUpdate());
            btnDelete.setEnabled(menuAuth.canDelete());
        }
    }

    private void udfNew() {
        MasterItem master = new MasterItem(JOptionPane.getFrameForComponent(this), true);
        master.setConn(conn);
        master.setSourceForm(this);
        master.setVisible(true);
    }

    private void udfEdit() {
        int iRow = jTable1.getSelectedRow();
        if (iRow < 0) {
            return;
        }
        Integer id = Integer.valueOf(jTable1.getValueAt(iRow, jTable1.getColumnModel().getColumnIndex("ID")).toString());
        try {
            MasterItem master = new MasterItem(JOptionPane.getFrameForComponent(this), true);
            master.setConn(conn);
            master.setSourceForm(this);
            master.tampilkanItem(id);
            master.setVisible(true);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage());
        }

    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }

    public class MyKeyListener extends KeyAdapter {

        @Override
        public void keyTyped(java.awt.event.KeyEvent evt) {

//            if(getTableSource()==null)
//                return;
            if (evt.getSource() instanceof JTextField
                    && ((JTextField) evt.getSource()).getName() != null
                    && ((JTextField) evt.getSource()).getName().equalsIgnoreCase("textEditor")) {
                fn.keyTyped(evt);

            }

        }

        public void keyPressed(KeyEvent evt) {
            Component ct = KeyboardFocusManager.getCurrentKeyboardFocusManager().getPermanentFocusOwner();
            int keyKode = evt.getKeyCode();
            switch (keyKode) {
                case KeyEvent.VK_ENTER: {
                    if (!(ct instanceof JTable)) {
                        if (!fn.isListVisible()) {
                            Component c = findNextFocus();
                            if (c == null) {
                                return;
                            }
                            c.requestFocus();
                        } else {
                            fn.lstRequestFocus();
                        }
                    }
                    break;
                }
                case KeyEvent.VK_DOWN: {
                    if (!(ct.getClass().getSimpleName().equalsIgnoreCase("JTABLE"))) {
                        if (!fn.isListVisible()) {
                            Component c = findNextFocus();
                            if (c == null) {
                                return;
                            }
                            c.requestFocus();
                        } else {
                            fn.lstRequestFocus();
                        }
                        break;
                    }
                }

                case KeyEvent.VK_UP: {
                    if (!(evt.getSource() instanceof JTable)) {
                        Component c = findPrevFocus();
                        c.requestFocus();
                    }
                    break;
                }
                case KeyEvent.VK_F2: {
                    udfNew();
                    break;
                }
                case KeyEvent.VK_F3: {
                    udfEdit();
                    break;
                }
                case KeyEvent.VK_DELETE: {
                    if (evt.getSource().equals(jTable1)) {
                        udfDelete();
                    }
                    break;

                }

            }
        }

//        @Override
//        public void keyReleased(KeyEvent evt){
//            if(evt.getSource().equals(txtDisc)||evt.getSource().equals(txtQty)||evt.getSource().equals(txtUnitPrice))
//                GeneralFunction.keyTyped(evt);
//        }
        public Component findNextFocus() {
            // Find focus owner
            Component c = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
            Container root = c == null ? null : c.getFocusCycleRootAncestor();

            if (root != null) {
                FocusTraversalPolicy policy = root.getFocusTraversalPolicy();
                Component nextFocus = policy.getComponentAfter(root, c);
                if (nextFocus == null) {
                    nextFocus = policy.getDefaultComponent(root);
                }
                return nextFocus;
            }
            return null;
        }

        public Component findPrevFocus() {
            // Find focus owner
            Component c = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
            Container root = c == null ? null : c.getFocusCycleRootAncestor();

            if (root != null) {
                FocusTraversalPolicy policy = root.getFocusTraversalPolicy();
                Component prevFocus = policy.getComponentBefore(root, c);
                if (prevFocus == null) {
                    prevFocus = policy.getDefaultComponent(root);
                }
                return prevFocus;
            }
            return null;
        }
    }

    public void tampilkanData(Integer id, String sCari) {
        try {
            String query = "select i.id, coalesce(i.plu,'') plu, i.nama_barang, coalesce(k.nama_kategori,'') kategori, coalesce(i.satuan,'') satuan, \n"
                    + "coalesce(r.nama_relasi,'') supplier_default, coalesce(i.active, false) active\n"
                    + "from m_item i \n"
                    + "left join m_item_kategori k on k.id=i.id_kategori\n"
                    + "left join m_relasi r on r.id_relasi=i.id_supp_default \n"
                    + "where i.id::varchar ilike '%" + sCari + "%' \n"
                    + "or nama_barang ilike '%" + sCari + "%' \n"
                    + "or coalesce(i.plu,'') = '" + sCari + "' \n"
                    + "order by i.nama_barang ";

            ((DefaultTableModel) jTable1.getModel()).setNumRows(0);
//            List<Item> list=itemDao.tampilkanData(sCari);
            int i = 0;
            ResultSet rs = MainForm.conn.createStatement().executeQuery(query);
            while (rs.next()) {
                ((DefaultTableModel) jTable1.getModel()).addRow(new Object[]{
                    rs.getInt("id"),
                    rs.getString("plu"),
                    rs.getString("nama_barang"),
                    rs.getString("kategori"),
                    rs.getString("satuan"),
                    rs.getString("supplier_default"),
                    rs.getBoolean("active"),});
                if (id == rs.getInt("id")) {
                    i = jTable1.getRowCount() - 1;
                }
            }
            if (jTable1.getRowCount() > 0) {
                jTable1.setRowSelectionInterval(i, i);
                jTable1.setModel(fn.autoResizeColWidth(jTable1, ((DefaultTableModel) jTable1.getModel())).getModel());
            }
        } catch (Exception ex) {
            Logger.getLogger(FrmListItemHargaJual.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void udfDelete() {
        int iRow = jTable1.getSelectedRow();
        if (iRow < 0) {
            return;
        }
        try {
            if (JOptionPane.showConfirmDialog(this, "Anda yakin untuk menghapus akun '" + jTable1.getValueAt(iRow, jTable1.getColumnModel().getColumnIndex("Nama Item")) + "'", "Hapus Item", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION) {
                return;
            }

            itemDao.delete(Integer.parseInt(jTable1.getValueAt(iRow, jTable1.getColumnModel().getColumnIndex("ID")).toString()));
            ((DefaultTableModel) jTable1.getModel()).removeRow(jTable1.getSelectedRow());

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage());
        }
    }

    public void udfUpdateHargaJual() {
        try {
            String ins = "";
            for (int i = 0; i < jTable2.getRowCount(); i++) {
                ins += ins.equalsIgnoreCase("") ? "" : " union all \n";
                ins += "select fn_m_item_update_harga_jual("
                        + fn.udfGetInt(jTable1.getValueAt(jTable1.getSelectedRow(), 0)) + ","
                        + fn.udfGetInt(jTable2.getValueAt(i, jTable2.getColumnModel().getColumnIndex("IdTipe"))) + ","
                        + fn.udfGetDouble(jTable2.getValueAt(i, jTable2.getColumnModel().getColumnIndex("Mark Up (%)"))) + ","
                        + fn.udfGetDouble(jTable2.getValueAt(i, jTable2.getColumnModel().getColumnIndex("Harga Jual"))) + ")";
            }
            System.out.println(ins);
            ResultSet rs = conn.createStatement().executeQuery(ins);
            if (rs.next()) {
                System.out.println("Sukses update harga");
            }
            rs.close();
            rs = null;

            JOptionPane.showMessageDialog(this, "Update harga jual sukses!");
        } catch (SQLException ex) {
            Logger.getLogger(FrmListItemHargaJual.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        btnDelete = new javax.swing.JButton();
        btnNew = new javax.swing.JButton();
        btnClose = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        txtCari = new javax.swing.JTextField();
        btnRefresh = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        txtTglBeliTerakhir2 = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txtHargaBeliTerakhir2 = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        txtStok2 = new javax.swing.JTextField();
        txtHppRata2 = new javax.swing.JTextField();
        jXHyperlink1 = new org.jdesktop.swingx.JXHyperlink();
        jXHyperlink2 = new org.jdesktop.swingx.JXHyperlink();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Daftar Stok");
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameOpened(evt);
            }
        });

        jTable1.setAutoCreateRowSorter(true);
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "PLU", "Nama Item", "Kategori", "Satuan", "Supplier Default", "Active"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jTable1.getTableHeader().setReorderingAllowed(false);
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/delete.png"))); // NOI18N
        btnDelete.setText("Hapus (Del)");
        btnDelete.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        jPanel1.add(btnDelete, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 5, 115, -1));

        btnNew.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/add.png"))); // NOI18N
        btnNew.setText("Baru (F2)");
        btnNew.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btnNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewActionPerformed(evt);
            }
        });
        jPanel1.add(btnNew, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 5, 100, -1));

        btnClose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/cancel.png"))); // NOI18N
        btnClose.setText("Close");
        btnClose.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCloseActionPerformed(evt);
            }
        });
        jPanel1.add(btnClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(525, 5, 75, -1));

        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/images/application_edit.png"))); // NOI18N
        btnEdit.setText("Ubah (F3)");
        btnEdit.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        jPanel1.add(btnEdit, new org.netbeans.lib.awtextra.AbsoluteConstraints(415, 5, 105, -1));

        jLabel2.setText("Pencarian :");

        txtCari.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCariKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCariKeyReleased(evt);
            }
        });

        btnRefresh.setText("Refresh");
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Harga Penjualan", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel17.setText("Tgl Beli Terakhir");
        jPanel4.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 110, 20));

        txtTglBeliTerakhir2.setEditable(false);
        txtTglBeliTerakhir2.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtTglBeliTerakhir2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel4.add(txtTglBeliTerakhir2, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 20, 110, 22));

        jLabel18.setText("Hrg Beli Terakhir");
        jPanel4.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 45, 110, 20));

        txtHargaBeliTerakhir2.setEditable(false);
        txtHargaBeliTerakhir2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtHargaBeliTerakhir2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel4.add(txtHargaBeliTerakhir2, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 45, 110, 22));

        jLabel19.setText("Hpp Rata");
        jPanel4.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 45, 80, 20));

        jLabel20.setText("Stok");
        jPanel4.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 20, 80, 20));

        txtStok2.setEditable(false);
        txtStok2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtStok2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel4.add(txtStok2, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 20, 110, 22));

        txtHppRata2.setEditable(false);
        txtHppRata2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtHppRata2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel4.add(txtHppRata2, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 45, 110, 22));

        jXHyperlink1.setText("Sesuaikan Harga Jual");
        jXHyperlink1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jXHyperlink1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jXHyperlink1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jXHyperlink1ActionPerformed(evt);
            }
        });
        jPanel4.add(jXHyperlink1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 70, 180, 20));

        jXHyperlink2.setText("Sesuaikan Harga Jual");
        jXHyperlink2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jXHyperlink2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jXHyperlink2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jXHyperlink2ActionPerformed(evt);
            }
        });
        jPanel4.add(jXHyperlink2, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 70, 160, 20));

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "IdTipe", "Tipe", "Mark Up (%)", "Harga Jual"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Double.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable2.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPane3.setViewportView(jTable2);

        jPanel4.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 100, 440, 160));

        jButton1.setText("Simpan Harga Jual");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 280, 180, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(txtCari, javax.swing.GroupLayout.PREFERRED_SIZE, 440, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 610, Short.MAX_VALUE))
                .addGap(10, 10, 10)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 460, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCari, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnRefresh))
                        .addGap(7, 7, 7)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addGap(0, 0, 0)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, 470, Short.MAX_VALUE)))
                .addGap(14, 14, 14))
        );

        setBounds(0, 0, 1116, 543);
    }// </editor-fold>//GEN-END:initComponents

    private void btnNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewActionPerformed
        udfNew();
    }//GEN-LAST:event_btnNewActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        udfDelete();
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnCloseActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        udfEdit();
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        txtCari.setText("");
        tampilkanData(0, "");
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void txtCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCariKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            tampilkanData(0, txtCari.getText());
        }
    }//GEN-LAST:event_txtCariKeyPressed

    private void formInternalFrameOpened(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameOpened
        initForm(conn);
    }//GEN-LAST:event_formInternalFrameOpened

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        if (evt.getClickCount() == 2 && jTable1.getSelectedRow() >= 0) {
            udfEdit();
        }
    }//GEN-LAST:event_jTable1MouseClicked

    private void txtCariKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCariKeyReleased
        tampilkanData(0, txtCari.getText());
    }//GEN-LAST:event_txtCariKeyReleased

    private void jXHyperlink2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jXHyperlink2ActionPerformed
        ((DefaultTableModel) jTable2.getModel()).removeTableModelListener(myTableModelListener);
        int colHarga = jTable2.getColumnModel().getColumnIndex("Harga Jual");
        int colMarkup = jTable2.getColumnModel().getColumnIndex("Mark Up (%)");
        double margin = 0, hpp = fn.udfGetDouble(txtHppRata2.getText());
        for (int i = 0; i < jTable2.getRowCount(); i++) {
            margin = fn.udfGetDouble(jTable2.getValueAt(i, colMarkup));
            jTable2.setValueAt(fn.roundUp(hpp * (1 + margin / 100), MainForm.roundUp), i, colHarga);
        }
        ((DefaultTableModel) jTable2.getModel()).addTableModelListener(myTableModelListener);
    }//GEN-LAST:event_jXHyperlink2ActionPerformed

    private void jXHyperlink1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jXHyperlink1ActionPerformed
        ((DefaultTableModel) jTable2.getModel()).removeTableModelListener(myTableModelListener);
        int colHarga = jTable2.getColumnModel().getColumnIndex("Harga Jual");
        int colMarkup = jTable2.getColumnModel().getColumnIndex("Mark Up (%)");
        double margin = 0, hargaTerakhir = fn.udfGetDouble(txtHargaBeliTerakhir2.getText());
        for (int i = 0; i < jTable2.getRowCount(); i++) {
            margin = fn.udfGetDouble(jTable2.getValueAt(i, colMarkup));
            jTable2.setValueAt(fn.roundUp(hargaTerakhir * (1 + margin / 100), MainForm.roundUp), i, colHarga);
        }
        ((DefaultTableModel) jTable2.getModel()).addTableModelListener(myTableModelListener);
    }//GEN-LAST:event_jXHyperlink1ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        udfUpdateHargaJual();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void tampilkanHargaJual(Integer id) {
        try {
            //tampilkan harga beli terakhir
            String query = "select * from fn_m_item_info_harga_stok(" + id + ") as (tgl date, harga_beli numeric, stok numeric, hpp_rata numeric)";
            System.out.println(query);
            ResultSet rs = MainForm.conn.createStatement().executeQuery(query);
            if (rs.next()) {
                if (rs.getDate("tgl") != null) {
                    txtTglBeliTerakhir2.setText(new SimpleDateFormat("dd/MM/yyyy").format(rs.getDate("tgl")));
                }

                txtHargaBeliTerakhir2.setText(fn.dFmt.format(rs.getDouble("harga_beli")));
                txtStok2.setText(fn.dFmt.format(rs.getDouble("stok")));
                txtHppRata2.setText(fn.dFmt.format(rs.getDouble("hpp_rata")));
            }
            rs.close();
            rs = null;
            //tampilkan margin
            query = "select t.id, t.nama, coalesce(h.margin, coalesce(t.margin_default,0)) margin, coalesce(h.harga_jual,0) harga\n"
                    + "from m_tipe_harga_jual t \n"
                    + "left join m_item_harga_jual h on h.id_tipe_harga_jual=t.id and h.id_item=" + id + "\n"
                    + "order by t.nama";
            System.out.println(query);
            ((DefaultTableModel) jTable2.getModel()).setNumRows(0);
            rs = MainForm.conn.createStatement().executeQuery(query);
            while (rs.next()) {
                ((DefaultTableModel) jTable2.getModel()).addRow(new Object[]{
                    rs.getInt("id"),
                    rs.getString("nama"),
                    rs.getDouble("margin"),
                    rs.getDouble("harga"),});
            }
            rs.close();
            rs = null;
        } catch (Exception ex) {
            Logger.getLogger(MasterItem2.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnNew;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private org.jdesktop.swingx.JXHyperlink jXHyperlink1;
    private org.jdesktop.swingx.JXHyperlink jXHyperlink2;
    private javax.swing.JTextField txtCari;
    private javax.swing.JTextField txtHargaBeliTerakhir2;
    private javax.swing.JTextField txtHppRata2;
    private javax.swing.JTextField txtStok2;
    private javax.swing.JTextField txtTglBeliTerakhir2;
    // End of variables declaration//GEN-END:variables

    public class MyTableCellEditor extends AbstractCellEditor implements TableCellEditor {

        private Toolkit toolkit;
        JTextComponent text = new JTextField() {

            @Override
            public void setFont(Font f) {
                super.setFont(new java.awt.Font("Tahoma", Font.BOLD, 11)); // NOI18N
            }

            protected boolean processKeyBinding(final KeyStroke ks, final KeyEvent e, final int condition, final boolean pressed) {
                if (hasFocus()) {
                    return super.processKeyBinding(ks, e, condition, pressed);
                } else {
                    this.requestFocus();

                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            processKeyBinding(ks, e, condition, pressed);
                        }
                    });
                    return true;
                }
            }
        };
        int col, row;
        private NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);

        public Component getTableCellEditorComponent(JTable table, Object value,
                boolean isSelected, int rowIndex, int vColIndex) {
            col = vColIndex;
            row = rowIndex;
            text.setBackground(new Color(0, 255, 204));
            text.addFocusListener(txtFocusListener);
            text.setFont(table.getFont());
            text.setName("textEditor");
            if (isSelected) {
            }

            if (value instanceof Double || value instanceof Float || value instanceof Integer) {
//                try {
                //Daouble dVal=Double.parseDouble(value.toString().replace(",",""));
                double dVal = fn.udfGetFloat(value.toString());
                text.setText(nf.format(dVal));
//                } catch (java.text.ParseException ex) {
//                    //Logger.getLogger(DlgLookupBarang.class.getName()).log(Level.SEVERE, null, ex);
//                }
            } else {
                text.setText(value == null ? "" : value.toString());
            }
            return text;
        }

        public Object getCellEditorValue() {
            Object retVal = 0;
            try {
                retVal = fn.udfGetDouble(((JTextField) text).getText());
                return retVal;
            } catch (Exception e) {
                System.out.println(e.getMessage());
                toolkit.beep();
                retVal = 0;
            }
            return retVal;
        }

    }

    private FocusListener txtFocusListener = new FocusListener() {
        public void focusGained(FocusEvent e) {
            if (e.getSource() instanceof JTextField || e.getSource() instanceof JFormattedTextField) {
                ((JTextField) e.getSource()).setBackground(Color.YELLOW);
                if ((e.getSource() instanceof JTextField && ((JTextField) e.getSource()).getName() != null && ((JTextField) e.getSource()).getName().equalsIgnoreCase("textEditor"))) {
                    ((JTextField) e.getSource()).setSelectionStart(0);
                    ((JTextField) e.getSource()).setSelectionEnd(((JTextField) e.getSource()).getText().length());

                }
            } else if (e.getSource() instanceof JXDatePicker) {
                ((JXDatePicker) e.getSource()).setBackground(Color.YELLOW);
            }
        }

        public void focusLost(FocusEvent e) {
            if (e.getSource() instanceof JTextField || e.getSource() instanceof JFormattedTextField) {
                ((JTextField) e.getSource()).setBackground(Color.WHITE);
            }
        }
    };
}
