/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ustasoft.pos.service;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cak-ust
 */
public class FakturHeader {

    private String namaToko;
    private String alamatToko1;
    private String alamatToko2;
    private String telpToko;
    private String noFaktur;
    private String tglFaktur;
    private String jamFaktur;
    private String statusBayar;
    private String namaPelanggan;
    private String alamatPelanggan;
    private String kotaPelanggan;
    private String telpPelanggan;
    private String keterangan;
    private String subTotal;
    private String diskon;
    private String nett;
    private String disiapkanOleh;
    private String dibuatOleh;
    private String diperiksaOleh;
    private String diterimaOleh;
    private List<FakturDetail> listItem = new ArrayList<FakturDetail>();

    public FakturHeader() {
    }

    public FakturHeader(String namaToko, String alamatToko1, String alamatToko2, String telpToko, String noFaktur, String tglFaktur, String jamFaktur, String statusBayar, String namaPelanggan, String alamatPelanggan, String kotaPelanggan, String telpPelanggan, String keterangan, String subTotal, String diskon, String nett, String disiapkanOleh, String dibuatOleh, String diperiksaOleh, String diterimaOleh) {
        this.namaToko = namaToko;
        this.alamatToko1 = alamatToko1;
        this.alamatToko2 = alamatToko2;
        this.telpToko = telpToko;
        this.noFaktur = noFaktur;
        this.tglFaktur = tglFaktur;
        this.jamFaktur = jamFaktur;
        this.statusBayar = statusBayar;
        this.namaPelanggan = namaPelanggan;
        this.alamatPelanggan = alamatPelanggan;
        this.kotaPelanggan = kotaPelanggan;
        this.telpPelanggan = telpPelanggan;
        this.keterangan = keterangan;
        this.subTotal = subTotal;
        this.diskon = diskon;
        this.nett = nett;
        this.disiapkanOleh = disiapkanOleh;
        this.dibuatOleh = dibuatOleh;
        this.diperiksaOleh = diperiksaOleh;
        this.diterimaOleh = diterimaOleh;
    }

    public String getNamaToko() {
        return namaToko;
    }

    public void setNamaToko(String namaToko) {
        this.namaToko = namaToko;
    }

    public String getAlamatToko1() {
        return alamatToko1;
    }

    public void setAlamatToko1(String alamatToko1) {
        this.alamatToko1 = alamatToko1;
    }

    public String getAlamatToko2() {
        return alamatToko2;
    }

    public void setAlamatToko2(String alamatToko2) {
        this.alamatToko2 = alamatToko2;
    }

    public String getNoFaktur() {
        return noFaktur;
    }

    public void setNoFaktur(String noFaktur) {
        this.noFaktur = noFaktur;
    }

    public String getTglFaktur() {
        return tglFaktur;
    }

    public void setTglFaktur(String tglFaktur) {
        this.tglFaktur = tglFaktur;
    }

    public String getJamFaktur() {
        return jamFaktur;
    }

    public void setJamFaktur(String jamFaktur) {
        this.jamFaktur = jamFaktur;
    }

    public String getStatusBayar() {
        return statusBayar;
    }

    public void setStatusBayar(String statusBayar) {
        this.statusBayar = statusBayar;
    }

    public String getNamaPelanggan() {
        return namaPelanggan;
    }

    public void setNamaPelanggan(String namaPelanggan) {
        this.namaPelanggan = namaPelanggan;
    }

    public String getAlamatPelanggan() {
        return alamatPelanggan;
    }

    public void setAlamatPelanggan(String alamatPelanggan) {
        this.alamatPelanggan = alamatPelanggan;
    }

    public String getKotaPelanggan() {
        return kotaPelanggan;
    }

    public void setKotaPelanggan(String kotaPelanggan) {
        this.kotaPelanggan = kotaPelanggan;
    }

    public String getTelpPelanggan() {
        return telpPelanggan;
    }

    public void setTelpPelanggan(String telpPelanggan) {
        this.telpPelanggan = telpPelanggan;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public String getDiskon() {
        return diskon;
    }

    public void setDiskon(String diskon) {
        this.diskon = diskon;
    }

    public String getNett() {
        return nett;
    }

    public void setNett(String nett) {
        this.nett = nett;
    }

    public String getDisiapkanOleh() {
        return disiapkanOleh;
    }

    public void setDisiapkanOleh(String disiapkanOleh) {
        this.disiapkanOleh = disiapkanOleh;
    }

    public String getDibuatOleh() {
        return dibuatOleh;
    }

    public void setDibuatOleh(String dibuatOleh) {
        this.dibuatOleh = dibuatOleh;
    }

    public String getDiperiksaOleh() {
        return diperiksaOleh;
    }

    public void setDiperiksaOleh(String diperiksaOleh) {
        this.diperiksaOleh = diperiksaOleh;
    }

    public String getDiterimaOleh() {
        return diterimaOleh;
    }

    public void setDiterimaOleh(String diterimaOleh) {
        this.diterimaOleh = diterimaOleh;
    }

    public List<FakturDetail> getListItem() {
        return listItem;
    }

    public void setListItem(List<FakturDetail> listItem) {
        this.listItem = listItem;
    }

    public void tambahItem(FakturDetail d){
        listItem.add(d);
    }

    public String getTelpToko() {
        return telpToko;
    }

    public void setTelpToko(String telpToko) {
        this.telpToko = telpToko;
    }
    
}
