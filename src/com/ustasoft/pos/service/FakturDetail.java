/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ustasoft.pos.service;

/**
 *
 * @author cak-ust
 */
public class FakturDetail {
    private String namaBarang;
    private String qty;
    private String satuan;
    private String harga;
    private String diskon;
    private String jumlah;

    public FakturDetail(String namaBarang, String qty, String satuan, String harga, String diskon, String jumlah) {
        this.namaBarang = namaBarang;
        this.qty = qty;
        this.satuan = satuan;
        this.harga = harga;
        this.diskon = diskon;
        this.jumlah = jumlah;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getSatuan() {
        return satuan;
    }

    public void setSatuan(String satuan) {
        this.satuan = satuan;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getDiskon() {
        return diskon;
    }

    public void setDiskon(String diskon) {
        this.diskon = diskon;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }
    
}
