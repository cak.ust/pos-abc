/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ustasoft.pos.service;

import com.ustasoft.pos.dao.jdbc.ArInvDao;
import java.awt.Component;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.DocFlavor;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.HashPrintServiceAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.PrintServiceAttributeSet;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.type.OrientationEnum;
import pos.MainForm;
import pos.ar.FrmPenjualan;

/**
 *
 * @author cak-ust
 */
public class PrintTrxService {

    private static PropertyResourceBundle resources;
    private Connection conn;
//    JRExporter exporter = new JRPrintServiceExporter(); 
    PrintService printService[];
    private boolean jenisPrinterKecilDotMatrix = true;
    private boolean jenisPrinterBesarDotMatrix = true;
    private String namaPrinterKecil = "";
    private String namaPrinterBesar = "";
    PrintU220 printU220 = new PrintU220();
    ArInvDao invDao = new ArInvDao();

    static {
        try {
            String sDir = System.getProperties().getProperty("user.dir");
            resources = new PropertyResourceBundle(new FileInputStream(new File(sDir + "/setting.properties")));
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        } catch (MissingResourceException mre) {
            System.err.println("setting.properties not found");
            System.exit(1);
        }
    }

    public PrintTrxService() {
        jenisPrinterKecilDotMatrix = resources.getString("jenis_printer_kecil") != null && resources.getString("jenis_printer_kecil").equalsIgnoreCase("1");
        jenisPrinterBesarDotMatrix = resources.getString("jenis_printer_besar") != null && resources.getString("jenis_printer_besar").equalsIgnoreCase("1");
        namaPrinterKecil = resources.getString("printer_kecil");
        namaPrinterBesar = resources.getString("printer_besar");
        invDao.setConn(MainForm.conn);
    }

    public void setConn(Connection c) {
        this.conn = c;
    }

    public void printJualLX300(Integer idPenjualan) {
        PrinterJob job = PrinterJob.getPrinterJob();
        //SysConfig sy=new SysConfig();

        DocFlavor flavor = DocFlavor.SERVICE_FORMATTED.PRINTABLE;
        PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
        PrintServiceAttributeSet pset = new HashPrintServiceAttributeSet();
        javax.print.PrintService[] services = PrintServiceLookup.lookupPrintServices(flavor, null);
        int i = 0;
        for (i = 0; i < services.length; i++) {
            if (services[i].getName().equalsIgnoreCase(resources.getString("printer_besar"))) {
                break;
            }
        }
        //if (JOptionPane.showConfirmDialog(null,"Cetak Invoice?","Message",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE)==JOptionPane.YES_OPTION) {
        try {
            PrintFakturJual pn = new PrintFakturJual(conn, idPenjualan, MainForm.sUserName, services[i]);
        } catch (java.lang.ArrayIndexOutOfBoundsException ie) {
            JOptionPane.showMessageDialog(null, "Printer tidak ditemukan!");
        }
    }

    public void printClosingTrx(String idPenjualan) {
        PrinterJob job = PrinterJob.getPrinterJob();
        //SysConfig sy=new SysConfig();

        DocFlavor flavor = DocFlavor.SERVICE_FORMATTED.PRINTABLE;
        PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
        PrintServiceAttributeSet pset = new HashPrintServiceAttributeSet();
        javax.print.PrintService[] services = PrintServiceLookup.lookupPrintServices(flavor, null);
        int i = 0;
        for (i = 0; i < services.length; i++) {
            if (services[i].getName().equalsIgnoreCase(resources.getString("printer_kwt"))) {
                break;
            }
        }
        //if (JOptionPane.showConfirmDialog(null,"Cetak Invoice?","Message",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE)==JOptionPane.YES_OPTION) {
        try {
            PrintClosing pn = new PrintClosing(conn, idPenjualan, MainForm.sUserName, services[i], false);
        } catch (java.lang.ArrayIndexOutOfBoundsException ie) {
            JOptionPane.showMessageDialog(null, "Printer tidak ditemukan!");
        }
    }

    public boolean cetakFakturPenjualan(Integer idPenjualan, Component aThis) {
        try {
            ResultSet rs = MainForm.conn.createStatement().executeQuery(
                    "select coalesce(r.id_tipe_harga_jual,1) "
                    + "from ar_inv i left join m_relasi r on r.id_relasi=i.id_customer "
                    + "where i.id=" + idPenjualan);

            if (rs.next()) {
//                if (rs.getInt(1) == 1) { //ECERAN
//                    if (jenisPrinterKecilDotMatrix) {
//                        printU220.cetak(idPenjualan);
//                        return true;
//                    } else {
//                        HashMap param = new HashMap();
//                        param.put("idPenjualan", idPenjualan);
//                        param.put("toko", MainForm.sToko);
//                        param.put("alamat1", MainForm.sAlamat1);
//                        param.put("alamat2", MainForm.sAlamat2);
//                        param.put("telp", MainForm.sTelp1);
//                        param.put("email", MainForm.sEmail);
//                        ReportService service = new ReportService(conn, aThis);
//                        service.setNamaPrinter(namaPrinterKecil);
//                        
//                        try {
//                            service.directPrint(param, "FakturPenjualanThermal", OrientationEnum.PORTRAIT);
//                        } catch (JRException ex) {
//                            JOptionPane.showMessageDialog(aThis, "Printer tidak ditemukan!");
//                        }
////                        int itemCount = invDao.itemCount(idPenjualan);
//                        return true;
//                    }
//                } else {
                    HashMap param = new HashMap();
                    param.put("idPenjualan", idPenjualan);
                    param.put("toko", MainForm.sToko);
                    param.put("alamat1", MainForm.sAlamat1);
                    param.put("alamat2", MainForm.sAlamat2);
                    param.put("telp", MainForm.sTelp1);
                    param.put("email", MainForm.sEmail);
                    int itemCount = invDao.itemCount(idPenjualan);
                    System.out.println("itemCount: " + itemCount);
                    param.put("IS_IGNORE_PAGINATION", itemCount > 6 || itemCount < 11);

                    new ReportService(conn, aThis).previewPenjualan(param);
//                    new ReportService(MainForm.conn, aThis).previewInWord2(param, "FakturPenjualan", OrientationEnum.PORTRAIT);
//                    new ReportService(MainForm.conn, aThis).previewInWord(param, "FakturPenjualan", OrientationEnum.PORTRAIT);
//                    new ReportService(conn, aThis).cetakPenjualanLX300(idPenjualan);
//                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(FrmPenjualan.class.getName()).log(Level.SEVERE, null, ex);
        }

        return true;
    }
}
