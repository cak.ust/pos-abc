/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ustasoft.pos.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import pos.MainForm;

/**
 *
 * @author cak-ust
 */
public class PenjualanService {
    private Connection conn=null;
    
    public PenjualanService(){
        
    }
    public PenjualanService(Connection c){
        this.conn=c;
    }
    public void settingPenjualan(){
        ResultSet rs;
        try {
            rs = conn.createStatement().executeQuery("select coalesce(foot_note_faktur,'') as foot_note_faktur "
                    + "from m_setting_penjualan");
            if(rs.next()){
               MainForm.footNoteFaktur = rs.getString("foot_note_faktur");
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(PenjualanService.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }
}
