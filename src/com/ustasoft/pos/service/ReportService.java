/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ustasoft.pos.service;

import com.sun.tools.javac.main.Main;
import com.ustasoft.component.OSValidator;
import com.ustasoft.pos.dao.jdbc.ArInvDao;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.PropertyResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.HashAttributeSet;
import javax.print.attribute.standard.PrinterName;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.engine.export.JRPrintServiceExporter;
import net.sf.jasperreports.engine.export.JRPrintServiceExporterParameter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.type.OrientationEnum;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import pos.MainForm;

/**
 *
 * @author cak-ust
 */
public class ReportService {

    private JFileChooser fileChooser = new JFileChooser();
    private ArInvDao invDao = new ArInvDao();
    private final Connection conn;
    private Component aThis;
    JRExporter exporter = new JRPrintServiceExporter();
    private static PropertyResourceBundle resources;
    PrintService printService[];

    static {
        try {
            String sDir = System.getProperties().getProperty("user.dir");
            resources = new PropertyResourceBundle(new FileInputStream(new File(sDir + "/setting.properties")));
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        } catch (MissingResourceException mre) {
            System.err.println("setting.properties not found");
            System.exit(1);
        }
    }
    private String printerName;

    public ReportService(Connection con, Component comp) {
        this.conn = con;
        aThis = comp;
        invDao.setConn(con);

    }

    public void setNamaPrinter(String printerName) {
        this.printerName = printerName;
        //then find the Printer Service   
        AttributeSet aset = new HashAttributeSet();
        aset.add(new PrinterName(printerName, null));
        printService = PrintServiceLookup.lookupPrintServices(null, aset);

        if (printService == null) {
            //error message          
            JOptionPane.showMessageDialog(null, "No Printer Attached / Shared to the server");
        }
    }

    public void udfPreview(HashMap reportParam, String sReport, OrientationEnum orientationEnumValue) {
        try {
            if (aThis != null) {
                aThis.setCursor(new Cursor(Cursor.WAIT_CURSOR));
            }
            JasperReport jasperReport = null;
            jasperReport = (JasperReport) JRLoader.loadObject(getClass().getResourceAsStream("reports/" + sReport + ".jasper"));
            jasperReport.setProperty("net.sf.jasperreports.awt.ignore.missing.font", "true");
            JasperPrint print = JasperFillManager.fillReport(jasperReport, reportParam, conn);
//            JasperPrintManager.printReport(print,false);

            if (aThis != null) {
                aThis.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }
            if (print.getPages().isEmpty()) {
                JOptionPane.showMessageDialog(aThis, "Report tidak ditemukan!");
                return;
            }
            if (orientationEnumValue != null) {
                print.setOrientation(orientationEnumValue);
            }
            JasperViewer.viewReport(print, false);
        } catch (JRException je) {
            System.out.println(je.getMessage());
            aThis.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }
    }

    public void previewInPdf(HashMap reportParam, String sReport, OrientationEnum orientationEnumValue) {
        try {
            sReport = getClass().getResource("reports/" + sReport + ".jasper").toURI().getPath();
            File tmp = File.createTempFile("Print", ".pdf");
            String outFileName = tmp.toString();
            System.out.println("Report : " + outFileName);
            System.out.println("Output : " + sReport);
            JasperPrint print = JasperFillManager.fillReport(sReport, reportParam, conn);

            if (print.getPages().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Report tidak ditemukan!");
                return;
            }
            JRExporter exporter = new net.sf.jasperreports.engine.export.JRPdfExporter();
            exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, outFileName.toString());
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
            exporter.exportReport();
            System.out.println("Created file: " + outFileName);
            Runtime rt = Runtime.getRuntime();
            try {

                String sPdf = "\"" + outFileName + "\"";

                rt.exec(new String[]{"cmd", "/c", outFileName});
            } catch (Exception e) {
                e.printStackTrace();
            }

            //setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        } catch (JRException ex) {
            Logger.getLogger(ReportService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReportService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(ReportService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void previewInWord2(HashMap reportParam, String sReport, OrientationEnum orientationEnumValue) {
        try {
            JasperReport jasperReport = null;
            jasperReport = (JasperReport) JRLoader.loadObject(getClass().getResourceAsStream("reports/" + sReport + ".jasper"));
            jasperReport.setProperty("net.sf.jasperreports.awt.ignore.missing.font", "true");

            File tmp = File.createTempFile("Print", ".doc");
            String outFileName = tmp.toString();
            JasperPrint print = JasperFillManager.fillReport(jasperReport, reportParam, MainForm.conn);
//            print.setPageHeight(396);
//            print.setPageWidth(728);

            if (print.getPages().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Report tidak ditemukan!");
                return;
            }
            aThis.setCursor(new Cursor(Cursor.WAIT_CURSOR));
            //JRExporter exporter = new net.sf.jasperreports.engine.export.JRPdfExporter();
            //JRExporter exporter = new JRDocxExporter();
            JRRtfExporter exporter = new JRRtfExporter();
            //JRDocxExporter exporter = new JRDocxExporter();
            exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, outFileName.toString());
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
            exporter.exportReport();
            System.out.println("Created file: " + outFileName);
            Runtime rt = Runtime.getRuntime();
            try {
                if (OSValidator.isWindows()) { //                    
                    rt.exec(new String[]{"cmd", "/c ", outFileName});
//                    rt.exec(new String[]{"cmd", "start /min winword /q /n /f /mFilePrint /mFileExit", outFileName});
                    //                    rt.exec(new String[]{"cmd", "start /min winword \"" + outFileName + "\" /q /n /f /mFilePrint /mFileExit"});
                    //                    rt.exec("start /min winword \"" + outFileName +"\" /q /n /f /mFilePrint /mFileExit");
//                {
//                    rt.exec("start /min winword ");

//                    rt.exec("cmd start /min C:\\\\Program Files\\Microsoft Office\\Office12\\winword \"" + outFileName + "\" /q /n /f /mFilePrint /mFileExit");
//                ProcessBuilder pb = new ProcessBuilder(
//                        "C:\\Program Files\\Microsoft Office\\Office12\\winword",
//                        "x",
//                        "myjar.jar",
//                        "*.*",
//                        "new");
//                pb.directory(new File("H:/"));
//                pb.redirectErrorStream(true);
//
//                Process p = pb.start();
                } else if (OSValidator.isUnix()) {
                    rt.exec("libreoffice " + outFileName);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            tmp.deleteOnExit();
            aThis.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        } catch (JRException ex) {
            Logger.getLogger(ReportService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReportService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    JasperReport jasperReportmkel = null;

    public void previewInWord(HashMap reportParam, String sReport, OrientationEnum orientationEnumValue) {
        try {
            AttributeSet aset = new HashAttributeSet();
            aset.add(new PrinterName(resources.getString("printer_besar"), null));
            printService = PrintServiceLookup.lookupPrintServices(null, aset);

            if (printService == null) {
                //error message          
                System.out.println("No Printer Attached / Shared to the server");
            }
            jasperReportmkel = (JasperReport) JRLoader.loadObject(getClass().getResourceAsStream("reports/" + sReport + ".jasper"));
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReportmkel, reportParam, conn);
//            jasperPrint.setOrientation(orientationEnumValue);
//            Paper paper = new Paper();
//            paper.setSize(10.11, 5.50);
//            PageFormat format = new PageFormat();
//            format.setPaper(paper);
            jasperPrint.setPageHeight(396);
            jasperPrint.setPageWidth(728);

            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRPrintServiceExporterParameter.PRINT_SERVICE_ATTRIBUTE_SET,
                    printService[0].getAttributes());
            exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PAGE_DIALOG, Boolean.FALSE);
            exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PRINT_DIALOG, Boolean.FALSE);
            exporter.exportReport();
        } catch (JRException ex) {
            Logger.getLogger(ReportService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void cetakPembelian(HashMap reportParam) {
        udfPreview(reportParam, "Pembelian", null);
//        previewInWord(reportParam, "Pembelian", null);
    }

    public void cetakPO(HashMap reportParam) {
        udfPreview(reportParam, "PO", null);
    }

    public void cetakSO(HashMap reportParam) {
        udfPreview(reportParam, "SO", null);
    }

    public void previewPenjualan(HashMap param) {
        //udfPreview(param, "FakturPenjualan", null);
        //previewInWord(param, "FakturPenjualan", null);
        //previewInPdf(param, "FakturPenjualan", null);
//        int itemCount = invDao.itemCount((Integer) param.get("idPenjualan"));
//        System.out.println("itemCount: " + itemCount);
//        param.put("IS_IGNORE_PAGINATION", itemCount <= 9);

//        previewInWord2(param, "FakturPenjualan", null);
//        previewInWord(param, "FakturPenjualan", null);
        udfPreview(param, "FakturPenjualan", null);
    }

    public void previewPenjualanThermal(HashMap param) {
        //udfPreview(param, "FakturPenjualan", null);
        //previewInWord(param, "FakturPenjualan", null);
        //previewInPdf(param, "FakturPenjualan", null);
//        int itemCount = invDao.itemCount((Integer) param.get("idPenjualan"));
//        System.out.println("itemCount: " + itemCount);
//        param.put("IS_IGNORE_PAGINATION", itemCount <= 9);

//        previewInWord2(param, "FakturPenjualan", null);
//        previewInWord(param, "FakturPenjualanThermal", null);
//        previewInPdf(param, "FakturPenjualanThermal", null);
        udfPreview(param, "FakturPenjualanThermal", null);
    }

    public void udfPreviewFakturPenjualan(Integer id){
        //printTrxService.cetakFakturPenjualan(id, aThis);
        HashMap param=new HashMap();
        param.put("idPenjualan", id);
        param.put("toko", MainForm.sToko);
        param.put("alamat1", MainForm.sAlamat1);
        param.put("alamat2", MainForm.sAlamat2);
        param.put("telp", MainForm.sTelp1);
        param.put("email", MainForm.sEmail);
        param.put("perhatian", MainForm.footNoteFaktur);
        int itemCount=invDao.itemCount(id);
        System.out.println("itemCount: "+itemCount);
//        param.put("IS_IGNORE_PAGINATION", itemCount>6 || itemCount<11);

        new ReportService(conn, aThis).previewPenjualan(param);
    }
    
    public void previewSJExpedisi(Integer idPenjualan){
        //printTrxService.cetakFakturPenjualan(id, aThis);
        HashMap param=new HashMap();
        param.put("idPenjualan", idPenjualan);
        param.put("toko", MainForm.sToko);
        param.put("alamat1", MainForm.sAlamat1);
        param.put("alamat2", MainForm.sAlamat2);
        param.put("telp", MainForm.sTelp1);
        param.put("email", MainForm.sEmail);
        int itemCount=invDao.itemCount(idPenjualan);

        new ReportService(conn, aThis).udfPreview(param, "SuratJalanExpedisi", OrientationEnum.PORTRAIT);
    }
    
    public void cetakPenjualanLX300(Integer id) {
        PrintTrxService p = new PrintTrxService();
        p.setConn(conn);
        p.printJualLX300(id);
    }

    public void cetakPenjualanU220(Integer id) {
        PrintU220 p = new PrintU220();
        p.cetak(id);
    }

    public void cetakSuratJalan(HashMap param) {
        udfPreview(param, "SuratJalan", OrientationEnum.PORTRAIT);
//        previewInWord2(param, "SuratJalan", OrientationEnum.PORTRAIT);
    }

    public void cetakReturPenjualan(HashMap param) {
        //udfPreview(param, "ReturPenjualan", null);
        previewInWord2(param, "ReturPenjualan", null);
    }

    public void cetakReturPembelian(HashMap param) {
        udfPreview(param, "PembelianRetur", null);
    }

    public void cetakKartuPiutang(HashMap param) {
        udfPreview(param, "KartuPiutang", null);
    }

    public void cetakKartuHutang(HashMap param) {
        udfPreview(param, "KartuHutang", null);
    }

    public void previewBuktiKas(String sNoBukti) {
        HashMap param = new HashMap();

        param.put("toko", MainForm.sToko);
        param.put("alamat1", MainForm.sAlamat1);
        param.put("alamat2", MainForm.sAlamat2);
        param.put("telp", MainForm.sTelp1);
        param.put("email", MainForm.sEmail);
        param.put("no_bukti", sNoBukti);
        param.put("logo", getClass().getResource("/resources/RM.jpg").toString());
        param.put("SUBREPORT_DIR", getClass().getResource("/com/ustasoft/pos/service/reports/").toString());
        //udfPreview(param, "BuktiKas", OrientationEnum.PORTRAIT);
        previewInWord2(param, "BuktiKas", OrientationEnum.PORTRAIT);
    }

    public void udfPreviewNeraca(String sTahun, String sBulan, Component aThis) {
        JTable tabel = getSkontroTable();
        String s = "select * from fn_acc_rpt_neraca('" + sBulan + "', '" + sTahun + "') as (groups text, tipe text, "
                + "acc_no varchar, acc_name varchar, lyear double precision, nyear double precision)";
        ((DefaultTableModel) tabel.getModel()).setNumRows(0);
        try {
            ResultSet rs = conn.createStatement().executeQuery(s);
            int i = 0, jmlBaris = 0, jmlAktiva = 0;
            while (rs.next()) {
                jmlBaris++;
                if (rs.getString("groups").equalsIgnoreCase("Aktiva")) {
                    ((DefaultTableModel) tabel.getModel()).addRow(new Object[]{
                        jmlBaris, rs.getString("acc_no"), rs.getString("acc_name"),
                        rs.getDouble("lYear"), rs.getDouble("nYear")
                    });
                } else {
                    if (jmlAktiva == 0) {
                        jmlAktiva = tabel.getRowCount();
                        i = 0;
                        jmlBaris = 0;
                    }
                    if (jmlAktiva <= i) {
                        ((DefaultTableModel) tabel.getModel()).addRow(new Object[]{
                            jmlBaris, null, null, null, null, rs.getString("acc_no"), rs.getString("acc_name"),
                            rs.getDouble("lYear"), rs.getDouble("nYear")
                        });
                    } else {
                        ((DefaultTableModel) tabel.getModel()).setValueAt(rs.getString("acc_no"), i, 5);
                        ((DefaultTableModel) tabel.getModel()).setValueAt(rs.getString("acc_name"), i, 6);
                        ((DefaultTableModel) tabel.getModel()).setValueAt(rs.getDouble("lYear"), i, 7);
                        ((DefaultTableModel) tabel.getModel()).setValueAt(rs.getDouble("nYear"), i, 8);
                    }
                    i++;
                }
            }
            HashMap reportParam = new HashMap();
            JasperReport jasperReport = null;
            reportParam.put("namaToko", MainForm.sToko);
            reportParam.put("alamat", MainForm.sAlamat1);
            reportParam.put("telp", MainForm.sTelp1);
            reportParam.put("tahun", sTahun);

            jasperReport = (JasperReport) JRLoader.loadObject(getClass().getResourceAsStream("reports/AccNeracaWithJTable.jasper"));
            JasperPrint print = JasperFillManager.fillReport(
                    jasperReport,
                    reportParam, new JRTableModelDataSource((DefaultTableModel) tabel.getModel()));

            print.setOrientation(jasperReport.getOrientationValue());
            aThis.setCursor(new Cursor(Cursor.WAIT_CURSOR));
            if (print.getPages().isEmpty()) {
                JOptionPane.showMessageDialog(aThis, "Data tidak ditemukan");
                return;
            }
            JasperViewer.viewReport(print, false);

        } catch (SQLException se) {
            JOptionPane.showMessageDialog(aThis, se.getMessage());
        } catch (JRException ex) {
            JOptionPane.showMessageDialog(aThis, ex.getMessage());
        }

    }

    private JTable getSkontroTable() {
        JTable jTable1 = new JTable();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{
                    "nomor", "AccNo_A", "AccName_A", "lYear_A", "nYear_A", "AccNo_P", "AccName_P", "lYear_P", "nYear_P"
                }
        ) {
            Class[] types = new Class[]{
                java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.Double.class, java.lang.Double.class, java.lang.String.class, java.lang.String.class, java.lang.Double.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean[]{
                false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });

        return jTable1;
    }

    void udfPreviewNeraca(HashMap param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void directPrint(HashMap reportParam, String sReport, OrientationEnum orientationEnumValue) throws JRException {
            AttributeSet aset = new HashAttributeSet();
            aset.add(new PrinterName(this.printerName, null));
            printService = PrintServiceLookup.lookupPrintServices(null, aset);

            if (printService == null) {
                //error message          
                JOptionPane.showMessageDialog(null, "No Printer Attached / Shared to the server");
            }
            jasperReportmkel = (JasperReport) JRLoader.loadObject(getClass().getResourceAsStream("reports/" + sReport + ".jasper"));
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReportmkel, reportParam, MainForm.conn);
            jasperPrint.setOrientation(orientationEnumValue);

            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRPrintServiceExporterParameter.PRINT_SERVICE_ATTRIBUTE_SET,
                    printService[0].getAttributes());
            exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PAGE_DIALOG, Boolean.FALSE);
            exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PRINT_DIALOG, Boolean.FALSE);
            exporter.exportReport();
    }
    File f = null;

    public void udfExportToExcel(String sQry, String namaFile, Component aThis) {
        try {
            Properties prop = System.getProperties();
        String sDir = prop.getProperty("user.dir");
        File dir = new File(sDir);
        HashMap reportParam = new HashMap();
//        if(jList1.getSelectedIndex()==0)
//            sFile=sFile.replace("$", "BukuHutangIPD-");
//        else if(jList1.getSelectedIndex()==1)
//            sFile=sFile.replace("$", "BukuHutangOPD-");

        fileChooser.addChoosableFileFilter(new MyFilter());
        fileChooser.setSelectedFile(new File(namaFile));
        int retval = fileChooser.showDialog(aThis, "Simpan File");

        if (retval == JFileChooser.APPROVE_OPTION) {
                namaFile = namaFile.indexOf(".xls") > 0 ? namaFile : namaFile + ".xls";

                f = new File(namaFile);

                WorkbookSettings ws = new WorkbookSettings();
                ws.setLocale(new Locale("en", "EN"));
                //WritableWorkbook workbook = Workbook.createWorkbook(fChoosen);
                WritableWorkbook workbook = Workbook.createWorkbook(new File(f.toString()), ws);
                WritableSheet sheet = workbook.createSheet("Sheet1", 0);

                WritableFont arial10font = new WritableFont(WritableFont.ARIAL, 10);
                WritableCellFormat arial10format = new WritableCellFormat(arial10font);

                WritableFont arial10fontBold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
                WritableCellFormat ariaBold10format = new WritableCellFormat(arial10fontBold);

                WritableFont arial12fontBold = new WritableFont(WritableFont.ARIAL, 12, WritableFont.BOLD);
                WritableCellFormat arial12format = new WritableCellFormat(arial12fontBold);

                //DateFormat customDateFormat = new DateFormat ("dd MMM yyyy hh:mm:ss");
                jxl.write.DateFormat customDateFormat = new jxl.write.DateFormat("dd/MM/yyyy");
                WritableCellFormat dateFormat = new WritableCellFormat(customDateFormat);

                WritableCellFormat cellNumberFmt = new WritableCellFormat(NumberFormats.THOUSANDS_FLOAT);

                WritableCellFormat cellIntFmt = new WritableCellFormat(NumberFormats.INTEGER);
                Number number2;
                aThis.setCursor(new Cursor(Cursor.WAIT_CURSOR));
                ResultSet rs = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(sQry);
                Label label;
                int row = 0;
                for (int i = 0; i < rs.getMetaData().getColumnCount(); i++) {
                    label = new Label(i, row, rs.getMetaData().getColumnName(i + 1), ariaBold10format);
                    sheet.addCell(label);
                }

                while (rs.next()) {
                    row++;
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        if (rs.getObject(i) != null && (rs.getMetaData().getColumnType(i) == java.sql.Types.VARCHAR || rs.getMetaData().getColumnType(i) == java.sql.Types.CHAR)) {
                            sheet.addCell(new Label(i - 1, row, rs.getString(i)));
                        } else if (rs.getMetaData().getColumnType(i) == java.sql.Types.DOUBLE && rs.getObject(i) != null) {
                            sheet.addCell(new jxl.write.Number(i - 1, row, rs.getDouble(i)));
                        } else if (rs.getMetaData().getColumnType(i) == java.sql.Types.NUMERIC && rs.getObject(i) != null) {
                            sheet.addCell(new jxl.write.Number(i - 1, row, rs.getDouble(i)));
                        } else if (rs.getMetaData().getColumnType(i) == java.sql.Types.FLOAT && rs.getObject(i) != null) {
                            sheet.addCell(new jxl.write.Number(i - 1, row, rs.getDouble(i)));
                        } else if (rs.getObject(i) != null && (rs.getMetaData().getColumnType(i) == java.sql.Types.INTEGER || rs.getMetaData().getColumnType(i) == java.sql.Types.BIGINT)) {
                            sheet.addCell(new jxl.write.Number(i - 1, row, rs.getInt(i)));
                        } else if (rs.getMetaData().getColumnType(i) == java.sql.Types.TIMESTAMP && rs.getObject(i) != null) {
                            sheet.addCell(new jxl.write.DateTime(i - 1, row, rs.getTimestamp(i), dateFormat));
                        } else if (rs.getMetaData().getColumnType(i) == java.sql.Types.DATE && rs.getObject(i) != null) {
                            sheet.addCell(new jxl.write.DateTime(i - 1, row, rs.getDate(i), dateFormat));
                        }
                    }

                }
                rs.close();

                workbook.write();
                workbook.close();
                if (JOptionPane.showConfirmDialog(aThis, "Export data sukses!\nSelanjutnya akan ditampilkan", "Tampilkan", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                    Runtime rt = Runtime.getRuntime();
                    try {

                        //String sExcel="\"c:\\POTONGAN 12.xls\"";
                        //String sExcel="\""+sDir+dir.separator+ "Test.xls\"";
                        //String sExcel="\""+sDir+dir.separator+"Tagihan "+sMasa+ ".xls\"";
                        String sExcel = "\"" + namaFile + "\"";

                        rt.exec(new String[]{"cmd", "/c", "start excel " + sExcel});
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                aThis.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }
        } catch (IOException ex) {
            Logger.getLogger(ReportService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ReportService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (WriteException ex) {
            Logger.getLogger(ReportService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public class MyFilter extends javax.swing.filechooser.FileFilter {

        public boolean accept(File file) {
            String filename = file.getName();
            return filename.endsWith(".xls");
        }

        public String getDescription() {
            return "*.xls";
        }

    }
}
