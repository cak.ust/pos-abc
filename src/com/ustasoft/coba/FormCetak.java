/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ustasoft.coba;

import java.awt.BorderLayout;
import java.awt.HeadlessException;
import java.math.BigDecimal;
import javax.swing.JFrame;
import simple.escp.Template;
import simple.escp.json.JsonTemplate;
import simple.escp.swing.PrintPreviewPane;

/**
 *
 * @author cak-ust
 */
public class FormCetak extends JFrame{

    public FormCetak() throws HeadlessException {
         super("Latihan");
 
        Faktur faktur = new Faktur("FA-1234-556677-XX-BB-CC");
        faktur.tambahItemFaktur(new ItemFaktur("Plantronics Backbeat Go 2 With Charging Case",
                1, new BigDecimal("13750000")));
        faktur.tambahItemFaktur(new ItemFaktur("CORT Gitar Akustik AD810 - Natural Satin",
                1, new BigDecimal("14900000")));
        faktur.tambahItemFaktur(new ItemFaktur("SAMSON Monitor Speaker System MediaOne 3A",
                1, new BigDecimal("14250000")));
 
        Template template  = null;
        try {
            template = new JsonTemplate(FormCetak.class.getResourceAsStream("template.json"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        PrintPreviewPane preview = new PrintPreviewPane(template, null, faktur);
        setLayout(new BorderLayout());
        add(preview, BorderLayout.CENTER);
 
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
    public static void main(String[] args) {
        new FormCetak();
    }
}
