/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ustasoft.coba;

/**
 *
 * @author cak-ust
 */
import java.math.BigDecimal;
 
public class ItemFaktur {
 
    private String namaBarang;
    private Integer qty;
    private BigDecimal harga;
 
    public ItemFaktur(String namaBarang, Integer qty, BigDecimal harga) {
        this.namaBarang = namaBarang;
        this.qty = qty;
        this.harga = harga;
    }
 
    public String getNamaBarang() {
        return namaBarang;
    }
 
    public Integer getQty() {
        return qty;
    }
 
    public BigDecimal getHarga() {
        return harga;
    }
 
}
