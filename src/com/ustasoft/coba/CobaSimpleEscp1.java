/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ustasoft.coba;

import simple.escp.SimpleEscp;
import simple.escp.Template;
import simple.escp.fill.FillJob;
import simple.escp.json.JsonTemplate;
import simple.escp.util.EscpUtil;

/**
 *
 * @author cak-ust
 */
public class CobaSimpleEscp1 {

    public static void main(String[] args) {
        try {
//            SimpleEscp simpleEscp = new SimpleEscp("EPSON LX-310 ESC/P");
            SimpleEscp simpleEscp = new SimpleEscp();
            Template template = new JsonTemplate(CobaSimpleEscp1.class.getResourceAsStream("template.json"));
            String hasil = new FillJob(template.parse()).fill();
            simpleEscp.print(hasil);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
